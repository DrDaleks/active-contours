package plugins.adufour.fdac.models;

import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.collection.array.Array1DUtil;

import java.util.concurrent.ExecutorService;

import plugins.adufour.fdac.contours.Contour2D;
import plugins.adufour.vars.lang.Var;

public abstract class DataModel2D<C extends Contour2D<?>> extends DataModel<C>
{
    protected final Var<Integer> slice;
    
    public DataModel2D(ExecutorService service, Var<Double> weight, Var<Sequence> sequence, Var<Integer> slice)
    {
        super(service, weight, sequence);
        this.slice = slice;
        
        // FIXME listener on slice
    }
    
    /**
     * Calculates the image value at the given real coordinates using 2D linear interpolation
     * 
     * @param x
     *            the X-coordinate of the point in double-precision
     * @param y
     *            the Y-coordinate of the point in double-precision
     * @param z
     *            the Z slice where the data must be sampled (<b>no interpolation</b>)
     * @param t
     *            the frame where the data must be sampled (<b>no interpolation</b>)
     * @param c
     *            the channel where the data must be sampled (<b>no interpolation</b>)
     * @return the interpolated image value at the given coordinates
     */
    protected double getPixelValue(double x, double y, int c, int t)
    {
        x /= resolutionX;
        y /= resolutionY;
        
        final int i = (int) Math.round(x);
        final int j = (int) Math.round(y);
        final int z = slice.getValue();
        
        if (i < 0 || i >= width - 1) return 0;
        if (j < 0 || j >= height - 1) return 0;
        
        IcyBufferedImage image = sequence.getValue().getImage(t, z);
        Object array = image.getDataXY(c);
        
        final double min = image.getChannelMin(c);
        final double max = image.getChannelMax(c);
        
        double value = 0;
        
        final int offset = i + j * width;
        final int offset_plus_1 = offset + 1; // saves 1 addition
        
        x -= i;
        y -= j;
        
        final double mx = 1 - x;
        final double my = 1 - y;
        
        // compute linear interpolation on the original data
        
        value += mx * my * Array1DUtil.getValue(array, offset, dataType);
        value += x * my * Array1DUtil.getValue(array, offset_plus_1, dataType);
        value += mx * y * Array1DUtil.getValue(array, offset + width, dataType);
        value += x * y * Array1DUtil.getValue(array, offset_plus_1 + width, dataType);
        
        // rescale to [0-1] on the fly
        return (value - min) / (max - min);
    }
}
