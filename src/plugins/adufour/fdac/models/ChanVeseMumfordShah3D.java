package plugins.adufour.fdac.models;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.concurrent.ExecutorService;

import icy.sequence.Sequence;
import icy.type.collection.array.Array1DUtil;
import plugins.adufour.fdac.contours.Contour3D;
import plugins.adufour.fdac.contours.OldControlPoint;
import plugins.adufour.fdac.geom.Vector3D;
import plugins.adufour.vars.lang.Var;

public class ChanVeseMumfordShah3D<C extends Contour3D<?>> extends DataModel3D<C>
{
    /**
     * The sequence channel where pixel values and mean intensities are computed
     */
    private final Var<Integer>             channel;
    
    private final Var<Boolean>             localizedMeans;
    
    private final Var<Integer>             nbIterationsBetweenUpadtes;
    
    /**
     * The mean image intensity inside each contour
     */
    private final LinkedHashMap<C, Double> means_in;
    
    /**
     * The mean data intensity outside each contour (or all contours, depending on whether means are
     * computed locally or globally)
     */
    private final LinkedHashMap<C, Double> means_out;
    
    /**
     * A labeled mask with the rasterized contours, useful for means computation
     */
    private final byte[][]                 mask_Z_XY;
    
    /**
     * The number of iterations this model has run (used to reduce the mean update rate to
     * computation time)
     */
    private int                            iteration = 0;
    
    /**
     * @param service
     *            the multi-thread service to run on
     * @param weight
     *            the weight of this model in the global deformation process
     * @param sequence
     *            the sequence to attach the model to
     * @param localized
     *            <code>true</code> to compute intensity means locally around the contour boundary,
     *            <code>false</code> to compute the means globally
     * @param nbIterationsBetweenUpdates
     *            the number of iterations to wait before updating the mean values inside/outside
     *            contours (set to 1 to update at every iteration)
     */
    public ChanVeseMumfordShah3D(ExecutorService service, Var<Double> weight, Var<Sequence> sequence, Var<Integer> channel, Var<Boolean> localized,
            Var<Integer> nbIterationsBetweenUpdates)
    {
        super(service, weight, sequence);
        this.channel = channel;
        this.means_in = new LinkedHashMap<C, Double>();
        this.means_out = new LinkedHashMap<C, Double>();
        this.localizedMeans = localized;
        this.nbIterationsBetweenUpadtes = nbIterationsBetweenUpdates;
        
        // create the labeled cache where contours will be rasterized
        mask_Z_XY = new byte[depth][width * height];
    }
    
    @Override
    public synchronized void registerMesh(C contour)
    {
        super.registerMesh(contour);
        means_in.put(contour, 1.0);
        means_out.put(contour, 0.0);
    }
    
    /**
     * @param t
     *            The time step on which means should be computed
     */
    public final void updateMeans(int t)
    {
        // erase the mask
        for (int z = 0; z < mask_Z_XY.length; z++)
            Arrays.fill(mask_Z_XY[z], (byte) 0);
        
        // compute the interior mean for each contour and fill the mask
        for (final C contour : means_in.keySet())
        {
            means_in.put(contour, contour.computeIntensity(sequence.getValue(), channel.getValue(), mask_Z_XY, multiThreadService));
        }
        
        // the mask is now filled, mean_out can be computed
        
        if (localizedMeans.getValue())
        {
            // compute the exterior mean for each contour in a local neighborhood
            
            Vector3D boxMin = new Vector3D();
            Vector3D boxMax = new Vector3D();
            Vector3D neighborhood = new Vector3D();
            
            for (final C contour : means_in.keySet())
            {
                // get the bounding box
                contour.boundingBox.getMinBounds(boxMin);
                contour.boundingBox.getMaxBounds(boxMax);
                
                // compute the neighborhood radius (half the bounding box)
                neighborhood.set(boxMax).subtract(boxMin).scale(0.5);
                
                // compute the "widened" bounding box
                boxMin.subtract(neighborhood);
                boxMax.add(neighborhood);
                
                // convert the bounding box to image coordinates
                boxMin.divide(resolutionX, resolutionY, resolutionZ);
                boxMax.divide(resolutionX, resolutionY, resolutionZ);
                
                // mind the edges!
                final int minX = Math.max(0, (int) Math.floor(boxMin.getX()));
                final int minY = Math.max(0, (int) Math.floor(boxMin.getY()));
                final int minZ = Math.max(0, (int) Math.floor(boxMin.getZ()));
                
                final int maxX = Math.min(width - 1, (int) Math.ceil(boxMax.getX()));
                final int maxY = Math.min(height - 1, (int) Math.ceil(boxMax.getY()));
                final int maxZ = Math.min(depth - 1, (int) Math.ceil(boxMax.getZ()));
                
                double outSum = 0, outCpt = 0;
                
                for (int k = minZ; k <= maxZ; k++)
                {
                    Object imageSlice = sequence.getValue().getDataXY(t, k, channel.getValue());
                    
                    byte[] maskSlice = mask_Z_XY[k];
                    
                    for (int j = minY; j <= maxY; j++)
                    {
                        int xyOffset = j * width + minX;
                        
                        for (int i = minX; i <= maxX; i++, xyOffset++)
                        {
                            if (maskSlice[xyOffset] == 0)
                            {
                                outCpt++;
                                outSum += Array1DUtil.getValue(imageSlice, xyOffset, dataType);
                            }
                        }
                    }
                }
                
                means_out.put(contour, outCpt == 0 ? 0 : outSum / outCpt);
            }
        }
        else
        {
            // use the same exterior mean for all contours
            double outSum = 0;
            int outCpt = 0;
            
            for (int z = 0; z < mask_Z_XY.length; z++)
            {
                Object imageSlice = sequence.getValue().getDataXY(t, z, channel.getValue());
                byte[] maskSlice = mask_Z_XY[z];
                
                for (int offset = 0; offset < maskSlice.length; offset++)
                    if (maskSlice[offset] == 0)
                    {
                        outSum += Array1DUtil.getValue(imageSlice, offset, dataType);
                        outCpt++;
                    }
            }
            
            // use the same exterior mean for all contours
            for (final C contour : means_out.keySet())
            {
                means_out.put(contour, outSum / outCpt);
            }
        }
    }
    
    @Override
    public void unregisterMesh(C contour)
    {
        super.unregisterMesh(contour);
        means_in.remove(contour);
    }
    
    @Override
    public void unregisterMeshes()
    {
        super.unregisterMeshes();
        means_in.clear();
    }
    
    @Override
    public void computeDeformations()
    {
        // update means at the required rate
        if (iteration % nbIterationsBetweenUpadtes.getValue() == 0) updateMeans(contours.get(0).getT());
        
        super.computeDeformations();
    }
    
    @Override
    public void computeDeformations(C contour)
    {
        Vector3D cvms = new Vector3D();
        double value, inDiff2, outDiff2;
        double mean_in = means_in.get(contour);
        double mean_out = means_out.get(contour);
        double w = weight.getValue();
        
        int t = contour.getT();
        int c = channel.getValue();
        
        // new: sensitivity is dependent on the data:
        // low mean_in => dim object => high sensitivity
        double sens = (1 / Math.max(mean_out * 2, mean_in));
        
        double resolution = contour.getResolution();
        
        for (OldControlPoint<Vector3D> controlPoint : contour)
        {
            if (controlPoint == null) continue;
            
            value = getPixelValue(controlPoint.position.getX(), controlPoint.position.getY(), controlPoint.position.getZ(), t, c);
            inDiff2 = value - mean_in;
            outDiff2 = value - mean_out;
            inDiff2 *= inDiff2;
            outDiff2 *= outDiff2;
            cvms.set(controlPoint.normal);
            cvms.scale(resolution * w * (sens * outDiff2 - inDiff2 / sens));
            
            controlPoint.drivingForces.add(cvms);
        }
    }
}
