package plugins.adufour.fdac.models;

import icy.sequence.Sequence;
import icy.type.DataType;

import java.util.concurrent.ExecutorService;

import plugins.adufour.fdac.contours.Contour;
import plugins.adufour.vars.lang.Var;

public abstract class DataModel<C extends Contour<?>> extends Model<C>
{
    protected final Var<Sequence> sequence;
    
    protected final int           width;
    
    protected final int           height;
    
    protected final int           depth;
    
    protected final double        resolutionX;
    
    protected final double        resolutionY;
    
    protected final double        resolutionZ;
    
    protected final DataType      dataType;
    
    public DataModel(ExecutorService service, Var<Double> weight, Var<Sequence> sequence)
    {
        super(service, weight);
        this.sequence = sequence;
        this.width = sequence.getValue().getWidth();
        this.height = sequence.getValue().getHeight();
        this.depth = sequence.getValue().getSizeZ();
        this.resolutionX = sequence.getValue().getPixelSizeX();
        this.resolutionY = sequence.getValue().getPixelSizeY();
        this.resolutionZ = sequence.getValue().getPixelSizeZ();
        this.dataType = sequence.getValue().getDataType_();
        
        // FIXME listener on sequence
    }
}
