package plugins.adufour.fdac.models;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import plugins.adufour.fdac.contours.Contour;
import plugins.adufour.fdac.contours.OldControlPoint;
import plugins.adufour.fdac.geom.Vector;

public class Coupling<C extends Contour<?>> extends Model<C>
{
    private final ArrayList<Callable<Integer>> couplingTasks = new ArrayList<Callable<Integer>>();
    
    public Coupling(ExecutorService service)
    {
        super(service, null); // no weight for coupling
    }
    
    @Override
    public void computeDeformations(final C contour)
    {
        Vector center = contour.getCenter();
        double radius = contour.boundingSphere.getRadius();
        
        couplingTasks.clear();
        couplingTasks.ensureCapacity(contours.size());
        
        synchronized (contours)
        {
            for (final C target : contours)
            {
                if (contour == target) continue;
                
                target.boundingSphere.update();
                
                if (center.getL2Distance(target.getCenter()) < radius + target.boundingSphere.getRadius())
                {
                    couplingTasks.add(new Callable<Integer>()
                    {
                        public Integer call()
                        {
                            return computeFeedback(contour, target);
                        }
                    });
                }
            }
        }
        
        try
        {
            multiThreadService.invokeAll(couplingTasks);
        }
        catch (InterruptedException e)
        {
            // restore the interrupted flag
            Thread.currentThread().interrupt();
        }
    }
    
    /**
     * Computes the feedback forces yielded by the penetration of the current contour into the
     * target contour
     * 
     * @param target
     *            the contour that is being penetrated
     * @return the number of vertices tested
     */
    private int computeFeedback(C contour, C target)
    {
        int tests = 0;
        
        double targetRadiusSq = target.boundingSphere.getRadius();
        targetRadiusSq *= targetRadiusSq;
        
        Vector targetCenter = target.getCenter();
        
        for (OldControlPoint<?> controlPoint : contour)
        {
            if (controlPoint == null) continue;
            
            double distanceSq = controlPoint.position.getL2DistanceSquared(targetCenter);
            
            if (distanceSq < targetRadiusSq)
            {
                tests++;
                
                double penetration = target.isColliding(controlPoint);
                
                if (penetration > 0)
                {
                    // couplingForce.scale(penetration * -1.0, v.normal);
                    // v.forces.add(couplingForce);
                    
                    // feedback is not strong enough
                    // => replace all forces by the feedback
                    
                    controlPoint.feedbackForces.set(controlPoint.normal).scale(penetration * -2.0);
                }
            }
        }
        
        return tests;
    }
}