package plugins.adufour.fdac.models;

import icy.plugin.abstract_.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import plugins.adufour.fdac.contours.Contour;
import plugins.adufour.vars.lang.Var;

/**
 * A model describes how a contour should deform. Models either link the contour to the local image
 * data (they are referred to as {@link DataModelC} models), or depend on the internal geometry
 * of a (or a set of) contours
 * 
 * @author Alexandre Dufour
 */
public abstract class Model<C extends Contour<?>> extends Plugin
{
    protected final ExecutorService multiThreadService;
    
    protected final Var<Double>     weight;
    
    protected final ArrayList<C>    contours = new ArrayList<C>();
    
    public Model(ExecutorService service, Var<Double> weight)
    {
        this.multiThreadService = service;
        this.weight = weight;
    }
    
    public void registerMesh(C contour)
    {
        synchronized (contours)
        {
            contours.add(contour);
        }
    }
    
    public void unregisterMesh(C contour)
    {
        synchronized (contours)
        {
            contours.remove(contour);
        }
    }
    
    public void unregisterMeshes()
    {
        synchronized (contours)
        {
            contours.clear();
        }
    }
    
    public void setMeshes(List<C> contour)
    {
        synchronized (contour)
        {
            contour.clear();
            this.contours.addAll(contour);
        }
    }
    
    /**
     * Computes deformations linked to this model for all registered meshes
     */
    public void computeDeformations()
    {
        for (C contour : contours)
            computeDeformations(contour);
    }
    
    public abstract void computeDeformations(C contour);
}
