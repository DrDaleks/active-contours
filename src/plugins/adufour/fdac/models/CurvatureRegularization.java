package plugins.adufour.fdac.models;

import java.util.List;
import java.util.concurrent.ExecutorService;

import plugins.adufour.fdac.contours.Contour;
import plugins.adufour.fdac.contours.OldControlPoint;
import plugins.adufour.fdac.geom.Vector3D;
import plugins.adufour.vars.lang.Var;

public class CurvatureRegularization<CP extends OldControlPoint<?>, C extends Contour<CP>> extends Model<C>
{
    public CurvatureRegularization(ExecutorService service, Var<Double> weight, boolean resolutionIndependent)
    {
        super(service, weight);
    }
    
    @Override
    public void computeDeformations(C contour)
    {
        Vector3D internalForce = new Vector3D();
        double w = weight.getValue() / contour.getResolution();
        
        if (w == 0.0) return;
        
        for (CP controlPoint : contour)
        {
            if (controlPoint == null) continue;
            
            List<CP> neighbors = contour.getNeighbors(controlPoint);
            
            internalForce.set(controlPoint.position).scale(-neighbors.size());
            
            for (CP neighbor : neighbors)
                internalForce.add(neighbor.position);
            
            internalForce.scale(w);
            
            controlPoint.drivingForces.add(internalForce);
        }
    }
}
