package plugins.adufour.fdac.models;

import java.util.concurrent.ExecutorService;

import icy.sequence.Sequence;
import plugins.adufour.fdac.contours.Contour3D;
import plugins.adufour.fdac.contours.OldControlPoint;
import plugins.adufour.fdac.geom.Vector3D;
import plugins.adufour.vars.lang.Var;

public class Gradient3D<C extends Contour3D<?>> extends DataModel3D<C>
{
    /**
     * The sequence channel where pixel values and mean intensities are computed
     */
    private final Var<Integer> channel;
    
    public Gradient3D(ExecutorService service, Var<Double> weight, Var<Sequence> sequence, Var<Integer> channel)
    {
        super(service, weight, sequence);
        
        this.channel = channel;
    }
    
    @Override
    public void computeDeformations(C contour)
    {
        double w = weight.getValue();
        
        final int t = contour.getT();
        final int c = channel.getValue();
        
        for (OldControlPoint<Vector3D> v : contour)
        {
            if (v == null) continue;
            
            double x = v.position.getX();
            double y = v.position.getY();
            double z = v.position.getZ();
            
            double val = getPixelValue(x, y, z, t, c);
            double dx = getPixelValue(x + 1, y, z, t, c);
            double dy = getPixelValue(x, y + 1, z, t, c);
            double dz = getPixelValue(x, y, z + 1, t, c);
            
            v.drivingForces.add(w * (dx - val), w * (dy - val), w * (dz - val));
        }
    }
}
