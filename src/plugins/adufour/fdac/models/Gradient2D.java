package plugins.adufour.fdac.models;

import java.util.concurrent.ExecutorService;

import icy.sequence.Sequence;
import plugins.adufour.fdac.contours.Contour2D;
import plugins.adufour.fdac.contours.OldControlPoint;
import plugins.adufour.fdac.geom.Vector2D;
import plugins.adufour.vars.lang.Var;

public class Gradient2D<C extends Contour2D<?>> extends DataModel2D<C>
{
    /**
     * The sequence channel where pixel values and mean intensities are computed
     */
    private final Var<Integer> channel;
    
    public Gradient2D(ExecutorService service, Var<Double> weight, Var<Sequence> sequence, Var<Integer> channel, Var<Integer> slice)
    {
        super(service, weight, sequence, slice);
        
        this.channel = channel;
    }
    
    @Override
    public void computeDeformations(C contour)
    {
        double w = weight.getValue();
        
        int t = contour.getT();
        int c = channel.getValue();
        
        for (OldControlPoint<Vector2D> v : contour)
        {
            if (v == null) continue;
            
            double x = v.position.getX();
            double y = v.position.getY();
            
            double val = getPixelValue(x, y, c, t);
            double dx = getPixelValue(x + 1, y, c, t);
            double dy = getPixelValue(x, y + 1, c, t);
            
            v.drivingForces.add(w * (dx - val), w * (dy - val));
        }
    }
}
