package plugins.adufour.fdac.geom;

import java.awt.geom.Line2D;

public class Vector2D extends Vector
{
    public Vector2D()
    {
        super(2);
    }
    
    public Vector2D(Vector2D vector)
    {
        super(vector);
    }
    
    public Vector2D(double x, double y)
    {
        super(x, y);
    }
    
    public double getX()
    {
        return values[0];
    }
    
    public double getY()
    {
        return values[1];
    }
    
    public Vector2D set(double x, double y)
    {
        return (Vector2D) super.set(x, y);
    }
    
    // static helper methods
    
    /**
     * Checks whether the segments [AB] and [CD] are intersecting
     * 
     * @param a
     *            the first component of segment [AB]
     * @param b
     *            the second component of segment [AB]
     * @param c
     *            the first component of segment [CD]
     * @param d
     *            the second component of segment [CD]
     * @return <code>true</code> if [AB] intersects [CD], <code>false</code> otherwise
     */
    public static boolean segmentsIntersect(Vector2D a, Vector2D b, Vector2D c, Vector2D d)
    {
        return Line2D.linesIntersect(a.values[0], a.values[1], b.values[0], b.values[1], c.values[0], c.values[1], d.values[0], d.values[1]);
    }
    
    /**
     * Projects the point P to the line directed by segment [AB], and computes the distance from P
     * to the line. It is therefore not guaranteed that the projection lies inside the segment [AB]
     * 
     * @param p
     *            the point to project
     * @param a
     *            the first component of segment [AB]
     * @param b
     *            the second component of segment [AB]
     * @return the distance from P to the line directed by segment [AB]
     */
    public static double getPointToLineDistance(Vector2D p, Vector2D a, Vector2D b)
    {
        return Line2D.ptLineDist(a.values[0], a.values[1], b.values[0], b.values[1], p.values[0], p.values[1]);
    }
}
