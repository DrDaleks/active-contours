package plugins.adufour.fdac.geom;

import plugins.adufour.fdac.contours.Contour;

public abstract class Bounds
{
    protected final Contour<?> contour;
    
    protected volatile boolean    upToDate;
    
    /**
     * Creates a new bounds structure
     * 
     * @param contour
     */
    public Bounds(Contour<?> contour)
    {
        this.contour = contour;
        this.upToDate = false;
    }
    
    /**
     * Updates the bounds information to the current contour. Note that the bounds are not
     * recomputed if they are already up to date
     */
    public void update()
    {
        if (upToDate) return;
        
        updateBounds();
        
        upToDate = true;
    }
    
    public void updateNeeded()
    {
        upToDate = false;
    }
    
    protected abstract void updateBounds();
}
