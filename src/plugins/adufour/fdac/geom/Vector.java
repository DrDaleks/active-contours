package plugins.adufour.fdac.geom;

/**
 * Class defining a N-Dimensional vector with common math operations
 * 
 * @author Alexandre Dufour
 */
public class Vector
{
    /**
     * This is the space origin and should not be modified (at least not directly)
     */
    private final Vector     zeroOrigin;
    
    public final int         dimension;
    
    protected final double[] values;
    
    public Vector(int dimension)
    {
        if (dimension <= 0) throw new IllegalArgumentException("Invalid vector dimension: " + dimension);
        this.dimension = dimension;
        this.zeroOrigin = new Vector(dimension);
        this.values = new double[dimension];
    }
    
    /**
     * @param dimension
     * @param initialValue
     *            the initial value for all components of this vector
     */
    public Vector(int dimension, double initialValue)
    {
        this(dimension);
        for (int d = 0; d < dimension; d++)
            values[d] = initialValue;
    }
    
    /**
     * Creates a new vector by copying the elements of the specified vector
     * 
     * @param vector
     */
    public Vector(Vector vector)
    {
        this(vector.dimension);
        set(vector);
    }
    
    public Vector(double... values)
    {
        this(values.length);
        set(values);
    }
    
    /**
     * Element-wise vector addition
     * 
     * @param vector
     *            the vector to add to this vector
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V add(double... vector)
    {
        for (int d = 0; d < dimension; d++)
            this.values[d] += vector[d];
        return (V) this;
    }
    
    /**
     * Element-wise vector addition
     * 
     * @param vector
     *            the vector to add to this vector
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V add(Vector vector)
    {
        return add(vector.values);
    }
    
    /**
     * Point-wise division of this vector with the specified vector
     * @param vector
     *            the vector to multiply
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V divide(V vector)
    {
        return divide(vector.values);
    }
    
    /**
     * Point-wise division of this vector with the specified vector
     * @param vector
     *            the vector to multiply
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V divide(double... vector)
    {
        for (int d = 0; d < dimension; d++)
            values[d] /= vector[d];
        return (V) this;
    }
    
    /**
     * @param vector
     * @return the dot product between this vector and the specified vector
     */
    public double dot(Vector vector)
    {
        double dot = 0.0;
        for (int d = 0; d < dimension; d++)
            dot += values[d] * vector.values[d];
        return dot;
    }
    
    public boolean equals(Vector vector)
    {
        for (int d = 0; d < dimension; d++)
            if (values[d] != vector.values[d]) return false;
        return true;
    }
    
    /**
     * @param dimension
     * @return the N-th element of this vector
     */
    public double get(int dimension)
    {
        return values[dimension];
    }
    
    /**
     * @param vector
     * @return the L1 (or Manhattan) distance from this vector to the specified vector
     */
    public double getL1Distance(Vector vector)
    {
        double distL1 = 0;
        for (int d = 0; d < dimension; d++)
            distL1 += Math.abs(this.values[d] - vector.values[d]);
        return distL1;
    }
    
    /**
     * @param vector
     * @return the squared L2 distance from this vector to the specified vector
     */
    public double getL2Distance(Vector vector)
    {
        return Math.sqrt(getL2DistanceSquared(vector));
    }
    
    /**
     * @param vector
     * @return the L2 distance from this vector to the specified vector
     */
    public double getL2DistanceSquared(Vector vector)
    {
        double distSquared = 0, t1, t2;
        for (int d = 0; d < dimension; d++)
        {
            t1 = this.values[d];
            t2 = vector.values[d];
            distSquared += (t1 - t2) * (t1 - t2);
        }
        return distSquared;
    }
    
    /**
     * @param vector
     * @param pNorm
     * @return the L-p distance from this vector to the specified vector
     */
    public double getDistance(Vector vector, int pNorm)
    {
        if (pNorm == 1) return getL1Distance(vector);
        
        if (pNorm == 2) return getL2Distance(vector);
        
        double distLp = 0, t1, t2;
        for (int d = 0; d < dimension; d++)
        {
            t1 = this.values[d];
            t2 = vector.values[d];
            distLp += Math.pow(t1 - t2, pNorm);
        }
        return Math.pow(distLp, 1.0 / pNorm);
    }
    
    /**
     * Computes the Lp norm of this vector.
     * 
     * @param p
     *            the norm to compute. Note that for <code>p=0</code>, the L0 pseudo-"norm" is
     *            computed as the number of non-zero elements of this vector (as defined by D.
     *            Donoho)
     * @return the Lp norm of this vector, i.e., the Lp distance to the N-space origin
     */
    public double getNorm(int p)
    {
        if (p < 0) return Double.NaN;
        
        switch (p)
        {
        case 0:
            // L0 pseudo-norm: counts the number of non-zero elements
            int norm0 = 0;
            for (double d : values)
                if (d != 0.0) norm0++;
            return norm0;
        case 1:
            return getL1Distance(zeroOrigin);
        case 2:
            return getL2Distance(zeroOrigin);
        default:
            return getDistance(zeroOrigin, p);
        }
    }
    
    /**
     * Replaces each vector value by the maximum value between this and the specified vector value
     * (for each dimension separately)
     * 
     * @param vector
     *            the vector to compute the minimum against
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V max(V vector)
    {
        for (int d = 0; d < dimension; d++)
            if (vector.values[d] > values[d]) values[d] = vector.values[d];
        return (V) this;
    }
    
    /**
     * Replaces each vector value by the minimum value between this and the specified vector value
     * (for each dimension separately)
     * 
     * @param vector
     *            the vector to compute the minimum against
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V min(V vector)
    {
        for (int d = 0; d < dimension; d++)
            if (vector.values[d] < values[d]) values[d] = vector.values[d];
        return (V) this;
    }
    
    /**
     * Point-wise multiplication of this vector with the specified vector
     * @param vector
     *            the vector to multiply
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V multiply(V vector)
    {
        return multiply(vector.values);
    }
    
    /**
     * Point-wise multiplication of this vector with the specified vector
     * @param vector
     *            the vector to multiply
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V multiply(double... vector)
    {
        for (int d = 0; d < dimension; d++)
            values[d] *= vector[d];
        return (V) this;
    }
    
    /**
     * Normalizes this vector, i.e., divides each element by the vector's Lp norm
     * 
     * @param pNorm
     *            the norm to use for normalization
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V normalize(int pNorm)
    {
        return scale(getNorm(pNorm));
    }
    
    /**
     * Resets this vector to the origin (a 0 value for each element)
     * 
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V reset()
    {
        return (V) set(zeroOrigin);
    }
    
    /**
     * Scales this vector by the specified factor
     * 
     * @param scaleFactor
     *            the scaling factor
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V scale(double scaleFactor)
    {
        for (int d = 0; d < dimension; d++)
            this.values[d] *= scaleFactor;
        return (V) this;
    }
    
    /**
     * Sets the new values of this vector by copying the specified elements
     * 
     * @param values
     *            the new vector elements to copy
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V set(double... values)
    {
        System.arraycopy(values, 0, this.values, 0, dimension);
        return (V) this;
    }
    
    /**
     * Sets the new value of this vector for the specified dimension
     * 
     * @param dimension
     *            the dimension to set
     * @param value
     *            the new element
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V set(int dimension, double value)
    {
        values[dimension] = value;
        return (V) this;
    }
    
    /**
     * Sets the new values of this vector by copying the specified vector
     * 
     * @param newvector
     *            the vector to copy the elements from
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V set(V newvector)
    {
        return set(newvector.values);
    }
    
    /**
     * Subtracts the values of the specified vector to this vector
     * 
     * @param vector
     *            the vector to subtract
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V subtract(Vector vector)
    {
        return subtract(vector.values);
    }
    
    /**
     * Subtracts the specified values to this vector
     * 
     * @param values
     *            the values to subtract
     * @return this modified vector (useful for chaining operations)
     */
    public <V extends Vector> V subtract(double... values)
    {
        for (int d = 0; d < dimension; d++)
            this.values[d] -= values[d];
        return (V) this;
    }
    
    public String toString()
    {
        String out = "(" + values[0];
        for (int d = 1; d < dimension; d++)
            out += ", " + values[d];
        out += ")";
        return out;
    }
}
