package plugins.adufour.fdac.geom;

import plugins.adufour.fdac.contours.Contour;
import plugins.adufour.fdac.contours.OldControlPoint;

public class BoundingSphere extends Bounds
{
    /**
     * The norm with which distances to the center should be computed
     */
    private final int    norm;
    
    private final Vector center;
    
    private double       radius;
    
    public BoundingSphere(Contour<?> contour, int norm)
    {
        super(contour);
        this.norm = norm;
        this.center = new Vector(contour.dimension);
        this.radius = 0.0;
        update();
    }
    
    public Vector getCenter()
    {
        update();
        return center;
    }
    
    public double getRadius()
    {
        update();
        return radius;
    }
    
    @Override
    protected void updateBounds()
    {
        center.reset();
        radius = 0.0;
        
        double nbVertices = 0;
        
        // FIXME synchronization
        // FIXME listener
        
        // compute the center first
        
        for (OldControlPoint<?> cp : contour)
            if (cp != null)
            {
                nbVertices++;
                center.add(cp.position);
            }
        
        if (nbVertices == 0) return;
        
        center.scale(1.0 / nbVertices);
        
        // now compute the maximum distance to the center
        
        for (OldControlPoint<?> cp : contour)
            if (cp != null)
            {
                double dist = cp.position.getDistance(center, norm);
                if (dist > radius) radius = dist;
            }
    }
}
