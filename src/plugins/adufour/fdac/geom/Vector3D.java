package plugins.adufour.fdac.geom;

public class Vector3D extends Vector
{
    public Vector3D()
    {
        super(3);
    }
    
    public Vector3D(Vector3D vector)
    {
        super(vector);
    }
    
    public Vector3D(double x, double y, double z)
    {
        super(x, y, z);
    }
    
    
    /**
     * Computes the vectorial (cross) product of this vector with the specified vector 
     * 
     * @param vector
     *            the vector to multiply with
     * @return this modified vector (useful for chaining operations)
     */
    public Vector3D cross(Vector3D vector)
    {
        return set(values[1] * vector.values[2] - values[2] * vector.values[1], values[2] * vector.values[0] - values[0] * vector.values[2], values[0] * vector.values[1] - values[1] * vector.values[0]);
    }
    
    public double getX()
    {
        return values[0];
    }
    
    public double getY()
    {
        return values[1];
    }
    
    public double getZ()
    {
        return values[2];
    }
    
    public Vector3D set(double x, double y, double z)
    {
        return (Vector3D) super.set(x, y, z);
    }
    
    public void setX(double x)
    {
        values[0] = x;
    }
    
    public void setY(double y)
    {
        values[1] = y;
    }
    
    public void setZ(double z)
    {
        values[2] = z;
    }
}
