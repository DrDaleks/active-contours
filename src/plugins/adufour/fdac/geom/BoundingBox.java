package plugins.adufour.fdac.geom;

import plugins.adufour.fdac.contours.Contour;
import plugins.adufour.fdac.contours.OldControlPoint;

public class BoundingBox extends Bounds
{
    /**
     * The minimum coordinate bounds
     */
    private final Vector minBounds;
    
    /**
     * The maximum coordinate bounds
     */
    public final Vector  maxBounds;
    
    public BoundingBox(Contour<?> contour)
    {
        super(contour);
        minBounds = new Vector(contour.dimension, Double.MAX_VALUE);
        maxBounds = new Vector(contour.dimension);
        update();
    }
    
    @Override
    protected void updateBounds()
    {
        // FIXME synchronization
        // FIXME listener
        
        for (OldControlPoint<?> cp : contour)
        {
            minBounds.min(cp.position);
            maxBounds.max(cp.position);
        }
    }
    
    public Vector getMinBounds()
    {
        update();
        return minBounds;
    }
    
    public void getMinBounds(Vector minBounds)
    {
        update();
        minBounds.set(this.minBounds);
    }
    
    public Vector getMaxBounds()
    {
        update();
        return maxBounds;
    }
    
    public void getMaxBounds(Vector maxBounds)
    {
        update();
        maxBounds.set(this.maxBounds);
    }
}
