package plugins.adufour.fdac.overlays;

import icy.canvas.Canvas2D;
import icy.canvas.Canvas3D;
import icy.canvas.IcyCanvas;
import icy.painter.Overlay;
import icy.sequence.Sequence;

import java.awt.Graphics2D;
import java.util.ArrayList;

import plugins.adufour.fdac.contours.polyhedron.Polyhedron;
import plugins.adufour.fdac.contours.polyhedron.VTKMesh;
import vtk.vtkActor;

public class VTKMeshPainter extends Overlay
{
    public VTKMeshPainter()
    {
        super("Mesh painter");
    }
    
    private final ArrayList<VTKMesh>  meshesToPaint = new ArrayList<VTKMesh>();
    
    private final ArrayList<vtkActor> actors        = new ArrayList<vtkActor>();
    
    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {
        if (canvas instanceof Canvas2D)
        {
            // TODO display rasterized view of the meshes on the 2D view
        }
        else if (canvas instanceof Canvas3D)
        {
            Canvas3D c3d = (Canvas3D) canvas;
            
            c3d.getRenderer().SetGlobalWarningDisplay(0);
            
            for (vtkActor actor : actors)
                c3d.getRenderer().RemoveActor(actor);
            
            actors.clear();
            
            synchronized (meshesToPaint)
            {
                for (VTKMesh mesh : meshesToPaint)
                {
                    if (mesh.mesh.getT() != canvas.getPositionT()) continue;
                    
                    mesh.update();
                    actors.add(mesh.actor);
                    c3d.getRenderer().AddActor(mesh.actor);
                }
            }
            
        }
    }
    
    /**
     * Adds the specified mesh to this painter
     * 
     * @param mesh
     */
    public void addMesh(Polyhedron mesh)
    {
        synchronized (meshesToPaint)
        {
            meshesToPaint.add(new VTKMesh(mesh));
        }
    }
    
    /**
     * Removes the specified mesh from this painter. No action is taken if the specified painter is
     * not registered in this painter
     * 
     * @param mesh
     */
    public void removeMesh(Polyhedron mesh)
    {
        if (!meshesToPaint.contains(mesh)) return;
        
        synchronized (meshesToPaint)
        {
            meshesToPaint.remove(mesh);
        }
    }
    
    public void reset()
    {
        meshesToPaint.clear();
    }
}
