package plugins.adufour.fdac.producers;

import java.util.ArrayList;

import plugins.adufour.fdac.contours.polyhedron.Face;
import plugins.adufour.fdac.contours.polyhedron.Polyhedron;
import plugins.adufour.fdac.contours.polyhedron.Polyhedron.PolyhedronTopologyOperator;
import plugins.adufour.fdac.contours.polyhedron.Vertex;
import plugins.adufour.fdac.geom.Vector3D;
import plugins.adufour.vars.lang.VarDouble;

public class Icosahedron extends MeshGenerator
{
    /**
     * The golden number
     */
    private static final double PHI = (1.0 + Math.sqrt(5.0)) / 2.0;
    
    private final double        radius;
    
    /**
     * Creates a new mesh initialized by a regular icosahedron of specified size, location and
     * resolution
     * 
     * @param radius
     *            the new mesh radius
     * @param center
     *            the new mesh center
     * @param resolution
     *            the mesh resolution
     */
    public Icosahedron(double radius, VarDouble resolution, VarDouble timeStep)
    {
        super(new Polyhedron(new ArrayList<Vertex>(12), new ArrayList<Face>(20), resolution, timeStep));
        this.radius = radius;
    }
    
    @Override
    public void run()
    {
        PolyhedronTopologyOperator topology = mesh.getTopology();
        
        // The icosahedron is defined by 12 vertices enclosed in a cube of size
        // 2PHI. All the Vertices of the icosahedron are located by pair on the
        // cube faces. Each of these vertices is designated by 2 letters 'xy'
        // where:
        // x is the orientation of the cube face containing the vertex
        // y is the orientation of the closest cube face to the vertex
        // Orientations are as follows: (positive = toward the screen)
        // w (west) : x = -PHI
        // e (east) : x = PHI
        // n (north) : y = PHI
        // s (south) : y = -PHI
        // t (top) : z = PHI
        // b (bottom): z = -PHI
        
        topology.beginUpdate(false);
        
        // Declaration of the vertices
        int zA = topology.addVertex(new Vector3D(PHI, 1, 0));
        int zB = topology.addVertex(new Vector3D(-PHI, 1, 0));
        int zC = topology.addVertex(new Vector3D(-PHI, -1, 0));
        int zD = topology.addVertex(new Vector3D(PHI, -1, 0));
        int yA = topology.addVertex(new Vector3D(1, 0, PHI));
        int yB = topology.addVertex(new Vector3D(1, 0, -PHI));
        int yC = topology.addVertex(new Vector3D(-1, 0, -PHI));
        int yD = topology.addVertex(new Vector3D(-1, 0, PHI));
        int xA = topology.addVertex(new Vector3D(0, PHI, 1));
        int xB = topology.addVertex(new Vector3D(0, -PHI, 1));
        int xC = topology.addVertex(new Vector3D(0, -PHI, -1));
        int xD = topology.addVertex(new Vector3D(0, PHI, -1));
        
        // Declaration of the faces
        topology.addFace(yA, xA, yD);
        topology.addFace(yA, yD, xB);
        topology.addFace(yB, yC, xD);
        topology.addFace(yB, xC, yC);
        
        topology.addFace(zA, yA, zD);
        topology.addFace(zA, zD, yB);
        topology.addFace(zC, yD, zB);
        topology.addFace(zC, zB, yC);
        
        topology.addFace(xA, zA, xD);
        topology.addFace(xA, xD, zB);
        topology.addFace(xB, xC, zD);
        topology.addFace(xB, zC, xC);
        
        topology.addFace(xA, yA, zA);
        topology.addFace(xD, zA, yB);
        topology.addFace(yA, xB, zD);
        topology.addFace(yB, zD, xC);
        topology.addFace(yD, xA, zB);
        topology.addFace(yC, zB, xD);
        topology.addFace(yD, zC, xB);
        topology.addFace(yC, xC, zC);
        
        // the radius of a sphere bounding a regular icosahedron is:
        // radius = 0.5 * resolution * SQRT( PHI * SQRT(5) )
        // here our definition implies "resolution = 2":
        double currentResolution = 2;
        double currentRadius = 0.5 * currentResolution * Math.sqrt(PHI * Math.sqrt(5));
        
        // scale the mesh to the desired size
        topology.scale(radius / currentRadius);
        
        topology.endUpdate();
    }
}
