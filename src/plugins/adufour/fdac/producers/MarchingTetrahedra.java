package plugins.adufour.fdac.producers;

import icy.sequence.Sequence;
import icy.type.collection.array.Array1DUtil;
import plugins.adufour.connectedcomponents.ConnectedComponent;
import plugins.adufour.fdac.contours.ContourException;
import plugins.adufour.fdac.contours.polyhedron.Polyhedron;
import plugins.adufour.fdac.contours.polyhedron.Polyhedron.PolyhedronTopologyOperator;
import plugins.adufour.fdac.geom.Vector3D;

public class MarchingTetrahedra extends MeshGenerator
{
    private final double[]                   data1D;
    private final int                        width, height, depth;
    private final double                     isoValue;
    private final int                        gridSize;
    private final Vector3D                   imageResolution;
    
    private final PolyhedronTopologyOperator topology = mesh.getTopology();
    
    public MarchingTetrahedra(Polyhedron mesh, Sequence s, int t, int c, double isoValue, double gridSizeInVoxels, Vector3D imageResolution, boolean useVTK)
    {
        super(mesh);
        
        mesh.setResolution(gridSizeInVoxels * imageResolution.getX());
        
        // get a copy of the volume to triangulate
        Object raw = s.getDataCopyXYZ(t, c);
        this.data1D = Array1DUtil.arrayToDoubleArray(raw, false);
        this.width = s.getSizeX();
        this.height = s.getSizeY();
        this.depth = s.getSizeZ();
        this.isoValue = isoValue;
        this.gridSize = (int) Math.round(gridSizeInVoxels);
        this.imageResolution = imageResolution;
    }
    
    public MarchingTetrahedra(Polyhedron mesh, ConnectedComponent object, double gridSizeInVoxels, Vector3D imageResolution, boolean useVTK)
    {
        super(mesh);
        
        mesh.setResolution(gridSizeInVoxels * imageResolution.getX());
        
        // get a copy of the volume to triangulate
        Sequence s = object.toSequence(); // FIXME SLOW IN MULTI-THREAD DUE TO PERSISTANCE !
        this.data1D = Array1DUtil.arrayToDoubleArray(s.getDataCopyXYZ(0, 0), false);
        this.width = s.getSizeX();
        this.height = s.getSizeY();
        this.depth = s.getSizeZ();
        this.isoValue = 0.5;
        this.gridSize = (int) Math.round(gridSizeInVoxels);
        this.imageResolution = imageResolution;
    }
    
    @Override
    public void run()
    {
        topology.beginUpdate(false);
        
        // pre-calculate some stuff
        int sliceSize = width * height;
        
        int gridSizeX = gridSize;
        int gridSizeY = gridSize;
        int gridSizeZ = Math.max(1, (int) Math.round(gridSize * imageResolution.getX() / imageResolution.getZ()));
        
        double gridWidth = imageResolution.getX() * gridSizeX;
        double gridHeight = imageResolution.getY() * gridSizeY;
        double gridDepth = imageResolution.getZ() * gridSizeZ;
        
        int gridWidthOffset = gridSizeX;
        int gridHeightOffset = gridSizeY * width;
        int gridDepthOffset = gridSizeZ * sliceSize;
        
        // erase first slices in all directions to ensure closed contours
        
        // along X
        for (int o = sliceSize; o < data1D.length; o += sliceSize)
            java.util.Arrays.fill(data1D, o, o + width, 0.0);
        
        // along Y
        for (int o = 0; o < data1D.length; o += width)
            data1D[o] = 0.0;
        
        // along Z
        java.util.Arrays.fill(data1D, 0, sliceSize, 0.0);
        
        int i, j, k;
        int iA, iB, iC, iD, iE, iF, iG, iH;
        Vector3D vA = new Vector3D();
        Vector3D vB = new Vector3D();
        Vector3D vC = new Vector3D();
        Vector3D vD = new Vector3D();
        Vector3D vE = new Vector3D();
        Vector3D vF = new Vector3D();
        Vector3D vG = new Vector3D();
        Vector3D vH = new Vector3D();
        double valA, valB, valC, valD, valE, valF, valG, valH;
        boolean outInX, outInY, outInZ;
        
        for (k = 0; k < depth; k += gridSizeZ)
        {
            outInZ = (k >= depth - gridSizeZ);
            
            double z = gridDepth * (k / gridSizeZ);
            vA.setZ(z);
            vB.setZ(z);
            vE.setZ(z);
            vF.setZ(z);
            
            z += gridDepth;
            vC.setZ(z);
            vD.setZ(z);
            vG.setZ(z);
            vH.setZ(z);
            
            for (j = 0; j < height; j += gridSizeY)
            {
                outInY = (j >= height - gridSizeY);
                
                double y = gridHeight * (j / gridSizeY);
                vA.setY(y);
                vB.setY(y);
                vC.setY(y);
                vD.setY(y);
                
                y += gridHeight;
                vE.setY(y);
                vF.setY(y);
                vG.setY(y);
                vH.setY(y);
                
                for (i = 0; i < width; i += gridSizeX)
                {
                    outInX = (i >= width - gridSizeX);
                    
                    double x = gridWidth * (i / gridSizeX);
                    vA.setX(x);
                    vE.setX(x);
                    vH.setX(x);
                    vD.setX(x);
                    
                    x += gridWidth;
                    vB.setX(x);
                    vF.setX(x);
                    vG.setX(x);
                    vC.setX(x);
                    
                    iA = i + j * width + k * sliceSize;
                    iB = iA + gridWidthOffset;
                    iC = iB + gridDepthOffset;
                    iD = iA + gridDepthOffset;
                    iE = iA + gridHeightOffset;
                    iF = iE + gridWidthOffset;
                    iG = iF + gridDepthOffset;
                    iH = iE + gridDepthOffset;
                    
                    valA = data1D[iA];
                    valB = outInX ? 0 : data1D[iB];
                    valC = outInX || outInZ ? 0 : data1D[iC];
                    valD = outInZ ? 0 : data1D[iD];
                    valE = outInY ? 0 : data1D[iE];
                    valF = outInX || outInY ? 0 : data1D[iF];
                    valG = outInX || outInY || outInZ ? 0 : data1D[iG];
                    valH = outInY || outInZ ? 0 : data1D[iH];
                    
                    double sum = valA + valB + valC + valD + valE + valF + valG + valH;
                    if (sum == 0 || sum == 8) continue;
                    
                    // extract the triangles from all 6 tetrahedra in the
                    // current cube
                    extractiso(isoValue, vC, valC, vH, valH, vD, valD, vB, valB);
                    extractiso(isoValue, vA, valA, vH, valH, vB, valB, vD, valD);
                    extractiso(isoValue, vG, valG, vB, valB, vH, valH, vC, valC);
                    extractiso(isoValue, vA, valA, vB, valB, vH, valH, vE, valE);
                    extractiso(isoValue, vB, valB, vE, valE, vF, valF, vH, valH);
                    extractiso(isoValue, vB, valB, vH, valH, vF, valF, vG, valG);
                }
            }
        }
        topology.endUpdate();
    }
    
    private void extractiso(double isoval, Vector3D v1, double c1, Vector3D v2, double c2, Vector3D v3, double c3, Vector3D v4, double c4) throws ContourException
    {
        int flag = 0;
        
        double d1, d2, d3, d4;
        
        d1 = c1 - isoval;
        d2 = c2 - isoval;
        d3 = c3 - isoval;
        d4 = c4 - isoval;
        
        if (d1 < 0.0f)
            flag |= 1;
        else if (d1 > 0.0f) flag |= 2;
        
        if (d2 < 0.0f)
            flag |= 4;
        else if (d2 > 0.0f) flag |= 8;
        
        if (d3 < 0.0f)
            flag |= 16;
        else if (d3 > 0.0f) flag |= 32;
        
        if (d4 < 0.0f)
            flag |= 64;
        else if (d4 > 0.0f) flag |= 128;
        
        switch (flag)
        {
        // isoval=c for three vertices
            case 1:
                extractiso1A(v1, v2, v3, v4);
            break;
            case 2:
                extractiso1A(v1, v2, v4, v3);
            break;
            case 4:
                extractiso1A(v2, v1, v4, v3);
            break;
            case 8:
                extractiso1A(v2, v1, v3, v4);
            break;
            case 16:
                extractiso1A(v3, v1, v2, v4);
            break;
            case 32:
                extractiso1A(v3, v1, v4, v2);
            break;
            case 64:
                extractiso1A(v4, v1, v3, v2);
            break;
            case 128:
                extractiso1A(v4, v1, v2, v3);
            break;
            
            // isoval=c for two vertices
            case 1 + 8:
                extractiso1B(v1, v2, v3, v4);
            break;
            case 2 + 4:
                extractiso1B(v1, v2, v4, v3);
            break;
            case 1 + 32:
                extractiso1B(v1, v3, v4, v2);
            break;
            case 2 + 16:
                extractiso1B(v1, v3, v2, v4);
            break;
            case 1 + 128:
                extractiso1B(v1, v4, v2, v3);
            break;
            case 2 + 64:
                extractiso1B(v1, v4, v3, v2);
            break;
            case 4 + 32:
                extractiso1B(v2, v3, v1, v4);
            break;
            case 8 + 16:
                extractiso1B(v2, v3, v4, v1);
            break;
            case 16 + 128:
                extractiso1B(v3, v4, v1, v2);
            break;
            case 32 + 64:
                extractiso1B(v3, v4, v2, v1);
            break;
            case 8 + 64:
                extractiso1B(v2, v4, v1, v3);
            break;
            case 4 + 128:
                extractiso1B(v2, v4, v3, v1);
            break;
            
            // isoval=c for one vertex
            case 4 + 32 + 64:
                extractiso1C(v4, v3, v2, v1);
            break;
            case 8 + 16 + 128:
                extractiso1C(v2, v3, v4, v1);
            break;
            case 4 + 16 + 128:
                extractiso1C(v2, v4, v3, v1);
            break;
            case 8 + 32 + 64:
                extractiso1C(v3, v4, v2, v1);
            break;
            case 8 + 16 + 64:
                extractiso1C(v3, v2, v4, v1);
            break;
            case 4 + 32 + 128:
                extractiso1C(v4, v2, v3, v1);
            break;
            case 1 + 32 + 64:
                extractiso1C(v1, v3, v4, v2);
            break;
            case 2 + 16 + 128:
                extractiso1C(v4, v3, v1, v2);
            break;
            case 1 + 16 + 128:
                extractiso1C(v3, v4, v1, v2);
            break;
            case 2 + 32 + 64:
                extractiso1C(v1, v4, v3, v2);
            break;
            case 2 + 16 + 64:
                extractiso1C(v4, v1, v3, v2);
            break;
            case 1 + 32 + 128:
                extractiso1C(v3, v1, v4, v2);
            break;
            case 1 + 8 + 64:
                extractiso1C(v4, v2, v1, v3);
            break;
            case 2 + 4 + 128:
                extractiso1C(v1, v2, v4, v3);
            break;
            case 1 + 4 + 128:
                extractiso1C(v1, v4, v2, v3);
            break;
            case 2 + 8 + 64:
                extractiso1C(v2, v4, v1, v3);
            break;
            case 2 + 4 + 64:
                extractiso1C(v2, v1, v4, v3);
            break;
            case 1 + 8 + 128:
                extractiso1C(v4, v1, v2, v3);
            break;
            case 1 + 8 + 16:
                extractiso1C(v1, v2, v3, v4);
            break;
            case 2 + 4 + 32:
                extractiso1C(v3, v2, v1, v4);
            break;
            case 1 + 4 + 32:
                extractiso1C(v2, v3, v1, v4);
            break;
            case 2 + 8 + 16:
                extractiso1C(v1, v3, v2, v4);
            break;
            case 2 + 4 + 16:
                extractiso1C(v3, v1, v2, v4);
            break;
            case 1 + 8 + 32:
                extractiso1C(v2, v1, v3, v4);
            break;
            
            // 1st case: isoval<c for one and isoval>c for other three vertices
            // 2nd case: isoval>c for one and isoval<c for other three vertices
            case 1 + 8 + 32 + 128:
                extractiso1D(v1, v2, v3, v4);
            break;
            case 2 + 4 + 16 + 64:
                extractiso1D(v1, v2, v4, v3);
            break;
            case 2 + 4 + 32 + 128:
                extractiso1D(v2, v1, v4, v3);
            break;
            case 1 + 8 + 16 + 64:
                extractiso1D(v2, v1, v3, v4);
            break;
            case 2 + 8 + 16 + 128:
                extractiso1D(v3, v1, v2, v4);
            break;
            case 1 + 4 + 32 + 64:
                extractiso1D(v3, v1, v4, v2);
            break;
            case 2 + 8 + 32 + 64:
                extractiso1D(v4, v1, v3, v2);
            break;
            case 1 + 4 + 16 + 128:
                extractiso1D(v4, v1, v2, v3);
            break;
            
            // 1st case: isoval<c for two and isoval>c for other two vertices
            // 2nd case: isoval>c for two and isoval<c for other two vertices
            case 1 + 4 + 32 + 128:
                extractiso2(v1, v2, v3, v4);
            break;
            case 2 + 8 + 16 + 64:
                extractiso2(v1, v2, v4, v3);
            break;
            case 1 + 8 + 16 + 128:
                extractiso2(v1, v3, v4, v2);
            break;
            case 2 + 4 + 32 + 64:
                extractiso2(v1, v3, v2, v4);
            break;
            case 2 + 4 + 16 + 128:
                extractiso2(v2, v3, v1, v4);
            break;
            case 1 + 8 + 32 + 64:
                extractiso2(v2, v3, v4, v1);
            break;
        }
    }
    
    private void extractiso1A(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4) throws ContourException
    {
        int p1 = topology.addVertex(v2);
        int p2 = topology.addVertex(v3);
        int p3 = topology.addVertex(v4);
        
        topology.addFace(p1, p2, p3);
    }
    
    private void extractiso1B(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4) throws ContourException
    {
        Vector3D v = new Vector3D(v1).add(v2).scale(0.5);
        
        int p1 = topology.addVertex(v);
        
        int p2 = topology.addVertex(v3);
        
        int p3 = topology.addVertex(v4);
        
        topology.addFace(p1, p2, p3);
    }
    
    private void extractiso1C(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4) throws ContourException
    {
        Vector3D v = new Vector3D();
        
        v.set(v1).add(v2).scale(0.5);
        
        int p1 = topology.addVertex(v);
        
        v.set(v2).add(v3).scale(0.5);
        
        int p2 = topology.addVertex(v);
        
        int p3 = topology.addVertex(v4);
        
        topology.addFace(p1, p2, p3);
    }
    
    private void extractiso1D(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4) throws ContourException
    {
        Vector3D v = new Vector3D();
        
        v.set(v1).add(v2).scale(0.5);
        
        int p1 = topology.addVertex(v);
        
        v.set(v1).add(v3).scale(0.5);
        
        int p2 = topology.addVertex(v);
        
        v.set(v1).add(v4).scale(0.5);
        
        int p3 = topology.addVertex(v);
        
        topology.addFace(p1, p2, p3);
    }
    
    private void extractiso2(Vector3D v1, Vector3D v2, Vector3D v3, Vector3D v4) throws ContourException
    {
        Vector3D v = new Vector3D();
        
        v.set(v1).add(v3).scale(0.5);
        
        int p1 = topology.addVertex(v);
        
        v.set(v1).add(v4).scale(0.5);
        
        int p2 = topology.addVertex(v);
        
        v.set(v2).add(v3).scale(0.5);
        
        int p3 = topology.addVertex(v);
        
        v.set(v2).add(v4).scale(0.5);
        
        int p4 = topology.addVertex(v);
        
        topology.addFace(p1, p2, p4);
        topology.addFace(p1, p4, p3);
    }
}
