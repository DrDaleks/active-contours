package plugins.adufour.fdac.producers;

import plugins.adufour.fdac.contours.polyhedron.Polyhedron;
import plugins.adufour.fdac.contours.polyhedron.Vertex;
import plugins.adufour.fdac.geom.Vector3D;

/**
 * Abstract class defining utilities to build mesh objects
 * 
 * @author Alexandre Dufour
 * 
 */
public abstract class MeshGenerator implements Runnable
{
	protected final Polyhedron	mesh;
	
	private boolean			produced	= false;
	
	protected MeshGenerator(Polyhedron mesh)
	{
		this.mesh = mesh;
	}
	
	/**
	 * Gets the mesh built by this producer. If the mesh is not yet built, the build method is
	 * called first and the final mesh is returned
	 * 
	 * @return
	 */
	public final Polyhedron getMesh()
	{
		return getMesh(null);
	}
	
	/**
	 * Gets the mesh built by this producer, and adds to it a 3D translation vector (in case the
	 * mesh must be placed somewhere). If the mesh is not yet built, the build method is called
	 * first and the final mesh is returned
	 * 
	 * @return
	 */
	public final Polyhedron getMesh(Vector3D offset)
	{
		if (!produced)
		{
			run();
			
			// apply the offset
			if (offset != null)
			{
				for (Vertex v : mesh)
				{
					if (v == null) continue;
					v.position.add(offset);
				}
			}
			
			produced = true;
		}
		return mesh;
	}
}
