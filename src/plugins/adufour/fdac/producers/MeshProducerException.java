package plugins.adufour.fdac.producers;

public class MeshProducerException extends RuntimeException
{
	private static final long	serialVersionUID	= 1L;

	public MeshProducerException(String message, Exception innerException)
	{
		super(message, innerException);
	}
}
