package plugins.adufour.fdac.producers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import plugins.adufour.fdac.contours.polyhedron.Polyhedron;
import plugins.adufour.fdac.geom.Vector3D;

public class VTKMeshReader extends MeshGenerator
{
    private final File vtkFile;
    
    public VTKMeshReader(Polyhedron mesh, File vtkFile)
    {
        super(mesh);
        this.vtkFile = vtkFile;
    }
    
    @Override
    public void run()
    {
        try
        {
            FileInputStream fis = new FileInputStream(vtkFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
            
            @SuppressWarnings("unused")
            String header = reader.readLine();
            @SuppressWarnings("unused")
            String description = reader.readLine();
            @SuppressWarnings("unused")
            String encoding = reader.readLine();
            @SuppressWarnings("unused")
            String dataType = reader.readLine();
            
            int nbPts = Integer.parseInt(reader.readLine().split(" ")[1]);
            
            for (int i = 0; i < nbPts; i++)
            {
                String[] vertex = reader.readLine().split(" ");
                mesh.getTopology().addVertex(new Vector3D(Double.parseDouble(vertex[0]), Double.parseDouble(vertex[1]), Double.parseDouble(vertex[2])));
            }
            
            int nbFaces = Integer.parseInt(reader.readLine().split(" ")[1]);
            
            double resolution = 0;
            
            for (int i = 0; i < nbFaces; i++)
            {
                String[] face = reader.readLine().split(" ");
                int v1 = Integer.parseInt(face[1]);
                int v2 = Integer.parseInt(face[2]);
                int v3 = Integer.parseInt(face[3]);
                mesh.getTopology().addFace(v1, v2, v3);
                
                resolution += mesh.getControlPoint(v1).position.getL2Distance(mesh.getControlPoint(v2).position);
                resolution += mesh.getControlPoint(v1).position.getL2Distance(mesh.getControlPoint(v3).position);
            }
            
            resolution /= nbFaces * 2;
            mesh.setResolution(resolution);
        }
        catch (Exception e)
        {
            throw new MeshProducerException("Error reading mesh from VTK file", e);
        }
    }
}
