package plugins.adufour.fdac.contours;

import plugins.adufour.fdac.geom.Vector;

/**
 * Class defining a N-dimensional contour control point
 * 
 * @author Alexandre Dufour
 */
public class OldControlPoint<V extends Vector>
{
    /**
     * The contour dimension (i.e. the number of coordinate components)
     */
    public final int dimension;
    
    /**
     * The position of this control point
     */
    public final V   position;
    
    /**
     * The normal vector to this control point pointing outwards
     */
    public final V   normal;
    
    /**
     * The forces that drive the control point according to user data
     */
    public final V   drivingForces;
    
    /**
     * The feedback forces applied to the control points (e.g. contour coupling)
     */
    public final V   feedbackForces;
    
    /**
     * Creates a new control point of specified dimension and zero coordinates
     * 
     * @param dimension
     */
    @SuppressWarnings("unchecked")
    public OldControlPoint(int dimension)
    {
        if (dimension <= 0) throw new IllegalArgumentException("Cannot create a dimension-less control point");
        
        this.dimension = dimension;
        this.position = (V) new Vector(dimension);
        this.drivingForces = (V) new Vector(dimension);
        this.normal = (V) new Vector(dimension);
        this.feedbackForces = (V) new Vector(dimension);
    }
    
    /**
     * Creates a new control point at the specified position
     * 
     * @param position
     */
    public OldControlPoint(V position)
    {
        this(position.dimension);
        this.position.set(position);
    }
    
    public OldControlPoint(OldControlPoint<V> cp)
    {
        this(cp.position);
        this.drivingForces.set(cp.drivingForces);
        this.feedbackForces.set(cp.feedbackForces);
        this.normal.set(cp.normal);
    }
    
    /**
     * Moves this control point by the specified displacement
     * 
     * @param displacement
     */
    public void move(Vector displacement)
    {
        this.position.add(displacement);
    }
    
    /**
     * Adds the specified force to the forces stored in this control point
     * 
     * @param force
     */
    public void addForce(double[] force)
    {
        this.drivingForces.add(force);
    }
    
    /**
     * Replace the forces currently stored in this control point by the specified force
     * 
     * @param force
     */
    public void setForce(double[] force)
    {
        this.drivingForces.set(force);
    }
    
    public String toString()
    {
        return "Control point: " + position.toString();
    }
}
