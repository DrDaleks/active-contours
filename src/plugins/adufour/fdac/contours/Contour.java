package plugins.adufour.fdac.contours;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;

import icy.canvas.IcyCanvas;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.util.GraphicsUtil;
import plugins.adufour.fdac.geom.BoundingBox;
import plugins.adufour.fdac.geom.BoundingSphere;
import plugins.adufour.fdac.geom.Vector;
import plugins.adufour.vars.lang.VarDouble;
import plugins.adufour.vars.lang.VarString;

/**
 * Class defining a generic N-dimensional discrete active contour using an unordered list of
 * {@link OldControlPoint}s. The connection between control points is not handled and left to extending
 * classes
 * 
 * @author Alexandre Dufour
 */
public abstract class Contour<CP extends OldControlPoint<? extends Vector>> implements Iterable<CP>
{
    public final int                 dimension;
    
    protected final List<CP>         controlPoints;
    
    public final VarString           id             = new VarString("ID", "" + hashCode());
    
    protected final VarDouble        resolution;
    
    protected final VarDouble        timeStep;
    
    protected final TopologyOperator topology;
    
    public final BoundingBox         boundingBox    = new BoundingBox(this);
    
    public final BoundingSphere      boundingSphere = new BoundingSphere(this, 2);
    
    private Color                    color;
    
    /**
     * Constructs a new active contour
     * 
     * @param dimension
     *            the contour dimension
     * @param timeStep
     *            the evolution time step
     * @param resolution
     *            the approximate distance in pixel units between 2 contour vertices. Note that any
     *            changes to this variable during deformation will immediately affect the contour
     */
    public Contour(int dimension, VarDouble resolution, VarDouble timeStep)
    {
        this(dimension, resolution, timeStep, 0);
    }
    
    /**
     * Constructs a new active contour with predefined number of points
     * 
     * @param dimension
     *            the contour dimension
     * @param timeStep
     *            the evolution time step
     * @param resolution
     *            the approximate distance in pixel units between 2 contour vertices. Note that any
     *            changes to this variable during deformation will immediately affect the contour
     * @param nbPoints
     *            the initial number of points (may vary later on)
     */
    public Contour(int dimension, VarDouble resolution, VarDouble timeStep, int nbPoints)
    {
        this(dimension, resolution, timeStep, new ArrayList<CP>(nbPoints));
    }
    
    /**
     * Constructs a new active contour with predefined set of control points
     * 
     * @param dimension
     *            the contour dimension
     * @param timeStep
     *            the evolution time step
     * @param resolution
     *            the approximate distance in pixel units between 2 contour vertices. Note that any
     *            changes to this variable during deformation will immediately affect the contour
     * @param controlPoints
     *            the initial control points (the list is not copied)
     */
    public Contour(int dimension, VarDouble resolution, VarDouble timeStep, List<CP> controlPoints)
    {
        this.dimension = dimension;
        this.resolution = resolution;
        this.timeStep = timeStep;
        this.controlPoints = controlPoints;
        this.topology = createTopologyOperator();
    }
    
    @Override
    public abstract Contour<CP> clone();
    
    /**
     * @param sequence
     *            the data where means are computed
     * @param t
     *            the time point where means are computed
     * @param c
     *            the channel where means are computed
     * @param byteMask
     *            the (optional) labeled mask used to store all meshes (set to <code>null</code> if
     *            not needed).
     * @param multithreadservice
     *            the service to run on
     * @return
     * @throws ContourException
     *             if the mesh is flattening and should be removed
     */
    public abstract double computeIntensity(final Sequence sequence, int channel, final Object byteMask, ExecutorService multiThreadService) throws ContourException;
    
    protected abstract TopologyOperator createTopologyOperator();
    
    /**
     * Flushes all computed deformations to the contour control points, and resets the deformations
     * for the next iteration. If the <code>field</code> parameter is used, the evolution is bound
     * to it, meaning that control points outside the specified field will not move.
     * 
     * @param field
     *            an optional field to which the contour evolution should be bounded (set to
     *            <code>null</code> if not wanted)
     */
    public void flushDeformations(ROI field)
    {
        double maxDisp = resolution.getValue() * timeStep.getValue();
        
        Vector finalForce = new Vector(dimension);
        
        synchronized (controlPoints)
        {
            for (CP cp : controlPoints)
            {
                // driving forces are applied only on the region of interest
                if (field != null && isInside(field, cp))
                {
                    finalForce.add(cp.drivingForces);
                }
                
                // feedback forces are applied all the time
                finalForce.add(cp.feedbackForces);
                
                // scale the force to the time step
                finalForce.scale(timeStep.getValue());
                
                // measure the total displacement of the control point
                double disp = finalForce.getNorm(2);
                
                if (disp > maxDisp) finalForce.scale(maxDisp / disp);
                
                cp.position.add(finalForce);
                
                finalForce.reset();
                cp.drivingForces.reset();
                cp.feedbackForces.reset();
            }
        }
        
        // recompute the contour normals
        updateNormals();
        boundingSphere.updateNeeded();
    }
    
    /**
     * @return the color
     */
    public Color getColor()
    {
        return color;
    }
    
    /**
     * @return the minimum volume under which the contour should be considered vanishing
     */
    public double getMinimumVolume()
    {
        return getResolution() * getResolution();
    }
    
    /**
     * @return the number of control points in this contour
     */
    public int getNbPoints()
    {
        return controlPoints.size();
    }
    
    /**
     * @return the perimeter of this contour
     */
    public abstract double getPerimeter();
    
    /**
     * @param algebraic
     *            <code>true</code> if the algebraic volume should be returned (may be negative),
     *            <code>false</code> otherwise
     * @return the volume of the interior of this contour
     */
    public abstract double getVolume(boolean algebraic);
    
    public Vector getCenter()
    {
        return boundingSphere.getCenter();
    }
    
    public CP getControlPoint(int i)
    {
        return controlPoints.get(i);
    }
    
    /**
     * @return the contour resolution (i.e. the approximate distance in pixel units between 2
     *         connected control points)
     */
    public double getResolution()
    {
        return resolution.getValue();
    }
    
    public abstract List<CP> getNeighbors(CP controlPoint);
    
    public TopologyOperator getTopology()
    {
        return topology;
    }
    
    /**
     * Checks whether the specified control point is in the specified region of interest. This can
     * be used for instance to forbid control points from moving out of the imaging (or any other
     * custom) field of view.
     * 
     * @param cp
     *            the control point to test
     * @return <code>true</code> if the specified control point lies within <code>field</code>,
     *         <code>false</code> otherwise
     */
    protected abstract boolean isInside(ROI field, CP cp);
    
    /**
     * @param v
     *            the position to test
     * @return 0 if the vertex is outside this mesh, otherwise a positive value measuring the
     *         penetration depth (in metrics unit)
     */
    public double isInside(Vector position)
    {
        OldControlPoint<Vector> v = new OldControlPoint<Vector>(position);
        
        // fake the vertex normal to point away from the center of this contour
        v.normal.subtract(getCenter());
        v.normal.set(position);
        v.normal.normalize(2);
        
        return isColliding(v);
    }
    
    /**
     * Checks whether the specified control point (usually from another contour) is colliding with
     * this contour, and returns the penetration measure (i.e. the closest distance from the
     * specified position to the boundary of this contour in the opposite of the control point's
     * normal direction)
     * 
     * @param v
     * @return
     */
    public abstract double isColliding(OldControlPoint<?> v);
    
    @Override
    public Iterator<CP> iterator()
    {
        return controlPoints.iterator();
    }
    
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {
        float fontSize = (float) canvas.canvasToImageLogDeltaX(30);
        g.setFont(new Font("Trebuchet MS", Font.BOLD, 10).deriveFont(fontSize));
        
        double stroke = Math.max(canvas.canvasToImageLogDeltaX(3), canvas.canvasToImageLogDeltaY(3));
        
        g.setStroke(new BasicStroke((float) stroke, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        
        g.setColor(getColor());
        
        GraphicsUtil.drawCenteredString(g, "" + id.getValue(), (int) getCenter().get(0), (int) getCenter().get(1), false);
    }
    
    /**
     * @param color
     *            the color to set
     */
    public void setColor(Color color)
    {
        this.color = color;
    }
    
    /**
     * Sets the resolution of this contour (i.e. the approximate distance in pixel units between 2
     * connected control points)
     * 
     * @param newResolution
     */
    public void setResolution(double newResolution)
    {
        resolution.setValue(newResolution);
    }
    
    public abstract void updateNormals();
}
