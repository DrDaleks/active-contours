package plugins.adufour.fdac.contours.polyhedron;

import java.util.ArrayList;

import plugins.adufour.fdac.contours.OldControlPoint;
import plugins.adufour.fdac.geom.Vector3D;

public class Vertex extends OldControlPoint<Vector3D>
{
    public final ArrayList<Integer> neighbors;
    
    public Vertex(Vertex v)
    {
        this(v.position, v.neighbors);
    }
    
    public Vertex(Vector3D position)
    {
        this(position, 0);
    }
    
    public Vertex(Vector3D position, int nbNeighbors)
    {
        super(position);
        
        this.neighbors = new ArrayList<Integer>(nbNeighbors);
    }
    
    public Vertex(Vector3D position, ArrayList<Integer> neighbors)
    {
        this(position, neighbors.size());
        for (Integer i : neighbors)
            this.neighbors.add(new Integer(i));
    }
    
    public String toString()
    {
        return super.toString() + " (" + neighbors.size() + " neighbors)";
    }
}
