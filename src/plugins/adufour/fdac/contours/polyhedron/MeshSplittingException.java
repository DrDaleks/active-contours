package plugins.adufour.fdac.contours.polyhedron;

import plugins.adufour.fdac.contours.ContourException;

/**
 * Class defining an exception which occurs when a contour is splitting during its evolution
 * 
 * @author Alexandre Dufour
 * 
 */
public class MeshSplittingException extends ContourException
{
	private static final long	serialVersionUID	= 1L;

	public final Polyhedron[]		children;
	
	public MeshSplittingException(Polyhedron contour, Polyhedron[] children)
	{
		super(contour, "Contour is splitting");
		this.children = children;
	}
}
