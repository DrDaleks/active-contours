package plugins.adufour.fdac.contours.polyhedron;

import java.util.ArrayList;

import plugins.adufour.fdac.geom.Vector3D;

/**
 * Structural element of triangular mesh
 * 
 * @author Alexandre Dufour
 */
public class Face
{
    Integer v1, v2, v3;
    
    /**
     * Constructs a new mesh face with given vertices indices. Note that vertices must be given in
     * counter-clockwise order.
     * 
     * @param v1
     *            the first vertex index
     * @param v2
     *            the second vertex index
     * @param v3
     *            the third vertex index
     */
    Face(Integer v1, Integer v2, Integer v3)
    {
        this.v1 = v1;
        this.v2 = v2;
        this.v3 = v3;
    }
    
    @Override
    protected Face clone()
    {
        return new Face(v1.intValue(), v2.intValue(), v3.intValue());
    }
    
    /**
     * Returns true if the specified vertex index is referred to by this face
     * 
     * @param v
     *            the vertex index to look for
     * @return true if the index is contained in this face, false otherwise
     */
    public boolean contains(Integer v)
    {
        return (v.compareTo(v1) == 0 || v.compareTo(v2) == 0 || v.compareTo(v3) == 0);
    }
    
    /**
     * Calculates the area of this face.
     * 
     * @param points
     *            - vertex list
     * @return the area of the face
     */
    public double getArea(ArrayList<Vertex> points)
    {
        Vector3D a = new Vector3D(points.get(v1).position);
        Vector3D b = new Vector3D(points.get(v2).position);
        
        a.subtract(points.get(v3).position);
        b.subtract(points.get(v3).position);
        
        return a.cross(b).getNorm(2) / 2;
    }
    
    /**
     * Calculates the area of this face with unit sphere pre-scaling.
     * 
     * @author Michael Reiner
     * @param points
     *            - vertex list
     * @return the area of the face
     */
    public double getArea(ArrayList<Vertex> points, Vector3D center)
    {
        Vector3D p1 = new Vector3D(points.get(v1).position);
        Vector3D p2 = new Vector3D(points.get(v2).position);
        Vector3D p3 = new Vector3D(points.get(v3).position);
        
        // if the vertices are not on the unit-sphere, project the points onto it (for correct
        // integral)
        p1.scale(1. / p1.getL2Distance(center));
        p2.scale(1. / p2.getL2Distance(center));
        p3.scale(1. / p3.getL2Distance(center));
        
        Vector3D a = new Vector3D(p1);
        Vector3D b = new Vector3D(p2);
        
        a.subtract(p3);
        b.subtract(p3);
        
        return a.cross(b).getNorm(2) / 2;
    }
    
    /**
     * Returns true if the specified vertex indices are ordered counter-clockwisely in the current
     * face. An exception is raised if the indices do not belong to this face
     * 
     * @param v1
     *            the first vertex
     * @param v2
     *            the second vertex
     * @return true if the edge v1-v2 is counter-clockwise for the current face, false otherwise
     * @throws IllegalArgumentException
     *             thrown if the specified vertex indices do not belong to this face
     */
    public boolean isCounterClockwise(Integer v1, Integer v2) throws IllegalArgumentException
    {
        if (v1.compareTo(this.v1) == 0)
        {
            if (v2.compareTo(this.v2) == 0) return true;
            
            if (v2.compareTo(this.v3) == 0) return false;
            
            throw new IllegalArgumentException("Vertex index " + v2 + " does not belong to this face");
        }
        
        if (v1.compareTo(this.v2) == 0)
        {
            if (v2.compareTo(this.v3) == 0) return true;
            
            if (v2.compareTo(this.v1) == 0) return false;
            
            throw new IllegalArgumentException("Vertex index " + v2 + " does not belong to this face");
        }
        
        if (v1.compareTo(this.v3) == 0)
        {
            if (v2.compareTo(this.v1) == 0) return true;
            
            if (v2.compareTo(this.v2) == 0) return false;
            
            throw new IllegalArgumentException("Vertex index " + v2 + " does not belong to this face");
        }
        
        throw new IllegalArgumentException("Vertex index " + v1 + " does not belong to this face");
    }
    
    public Vector3D[] getCoords(ArrayList<Vertex> points, Vector3D[] out)
    {
        if (out == null) out = new Vector3D[3];
        
        out[0] = points.get(v1).position;
        out[1] = points.get(v2).position;
        out[2] = points.get(v3).position;
        
        return out;
    }
    
    public String toString()
    {
        return "Face [" + v1 + "," + v2 + "," + v3 + "]";
    }
}
