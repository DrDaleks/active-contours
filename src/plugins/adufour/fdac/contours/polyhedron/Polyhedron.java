package plugins.adufour.fdac.contours.polyhedron;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import icy.sequence.Sequence;
import icy.system.thread.ThreadUtil;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import plugins.adufour.fdac.contours.Contour;
import plugins.adufour.fdac.contours.Contour3D;
import plugins.adufour.fdac.contours.ContourException;
import plugins.adufour.fdac.contours.OldControlPoint;
import plugins.adufour.fdac.contours.TopologyOperator;
import plugins.adufour.fdac.geom.Vector3D;
import plugins.adufour.vars.lang.VarDouble;

/**
 * Specialized implementation of a {@link Contour} representing a 3D polyhedron. The list of control
 * points is inherently not ordered (a control point always has more than 2 neighbors). Instead,
 * neighborhood information is stored through a list of faces and indexes in the control point array
 * 
 * @author Alexandre Dufour
 */
public class Polyhedron extends Contour3D<Vertex>
{
    final ArrayList<Callable<double[]>> meanUpdateTasks = new ArrayList<Callable<double[]>>();
    
    public final List<Face>             faces;
    
    // data used for ray-triangle intersections
    private final Vector3D              edge1           = new Vector3D();
    private final Vector3D              edge2           = new Vector3D();
    private final Vector3D              vp              = new Vector3D();
    private final Vector3D              vt              = new Vector3D();
    private final Vector3D              vq              = new Vector3D();
    private final Vector3D              ray             = new Vector3D();
    
    public Polyhedron(VarDouble resolution, VarDouble timeStep)
    {
        this(resolution, timeStep, 0, 0);
    }
    
    public Polyhedron(VarDouble resolution, VarDouble timeStep, int nbPoints, int nbFaces)
    {
        super(resolution, timeStep, 0);
        this.faces = new ArrayList<Face>(nbFaces);
    }
    
    /**
     * Creates a new mesh with the specified controlPoints and faces.
     * 
     * @param controlPoints
     *            the vertex array of the mesh
     * @param faces
     *            the face array of the mesh
     * @param resolution
     *            the default distance between two mesh controlPoints
     */
    public Polyhedron(List<Vertex> controlPoints, List<Face> faces, VarDouble resolution, VarDouble timeStep)
    {
        super(resolution, timeStep, controlPoints);
        
        this.faces = faces;
    }
    
    @Override
    public Polyhedron clone()
    {
        List<Vertex> clonedPoints = new ArrayList<Vertex>(controlPoints.size());
        for (Vertex v : controlPoints)
            clonedPoints.add(new Vertex(v));
        
        ArrayList<Face> clonedFaces = new ArrayList<Face>(this.faces.size());
        for (Face f : this.faces)
            clonedFaces.add(f.clone());
        
        return new Polyhedron(clonedPoints, clonedFaces, resolution, timeStep);
    }
    
    /**
     * @param packed
     *            <code>true</code> if the copy should be "packed" (the list of control points
     *            should not contain any null elements). This requires a slight extra processing
     *            time but saves memory for sparse polyhedra. It is also required to export in some
     *            file formats
     * @return a "packed" copy of the
     */
    public Polyhedron clone(boolean packed)
    {
        if (!packed) return clone();
        
        List<Vertex> clonedPoints = new ArrayList<Vertex>();
        
        List<Face> clonedFaces = new ArrayList<Face>(faces.size());
        for (Face f : faces)
            clonedFaces.add(f.clone());
        
        int lastNonNullIndex = 0;
        int nullCount = 0;
        
        reordering:
        for (int i = 0; i < controlPoints.size(); i++)
        {
            nullCount = 0;
            
            while (controlPoints.get(i) == null)
            {
                nullCount++;
                i++;
                if (i == controlPoints.size()) break reordering;
            }
            
            clonedPoints.add(new Vertex(controlPoints.get(i)));
            lastNonNullIndex++;
            
            if (nullCount != 0)
            {
                // some null controlPoints were found starting at index (i-nullCount)
                // subtract their number from all faces pointing to an index greater than the
                // previous known valid vertex
                for (Face f : clonedFaces)
                {
                    if (f.v1 >= lastNonNullIndex) f.v1 -= nullCount;
                    if (f.v2 >= lastNonNullIndex) f.v2 -= nullCount;
                    if (f.v3 >= lastNonNullIndex) f.v3 -= nullCount;
                }
            }
            // lastNonNullIndex = i;
        }
        
        return new Polyhedron(clonedPoints, clonedFaces, resolution, timeStep);
    }
    
    @Override
    protected TopologyOperator createTopologyOperator()
    {
        return new PolyhedronTopologyOperator();
    }
    
    public void exportToOFF(PrintStream ps)
    {
        Polyhedron packedMesh = clone(true);
        
        // OFF File format is written as follows:
        
        // line 1: "OFF"
        ps.println("OFF");
        
        // line 2: numVertices numFaces numEdges (numEdges is not important)
        ps.println(packedMesh.controlPoints.size() + " " + packedMesh.faces.size() + " 0");
        
        // one line per vertex "x y z"
        for (Vertex v : packedMesh)
            ps.println(v.position.getX() + " " + v.position.getY() + " " + v.position.getZ());
        
        // one line per face "numVertices v1 v2 ... vn"
        for (Face f : packedMesh.faces)
            ps.println("3 " + f.v1 + " " + f.v2 + " " + f.v3);
    }
    
    public void exportToVTK(PrintStream ps)
    {
        Polyhedron packedMesh = clone(true);
        
        // VTK File format is written as follows:
        
        // line 1: "# vtk DataFile Version x.x"
        ps.println("# vtk DataFile Version 3.0");
        
        // line 2: header information (256 chars max)
        ps.println("A great-looking mesh");
        
        // line 3: data format (one of ASCII or BINARY)
        ps.println("ASCII");
        
        // line 4: dataset structure "DATASET [OPTIONS]"
        ps.println("DATASET POLYDATA");
        
        // line 4: dataset type "POINTS number type"
        ps.println("POINTS " + packedMesh.controlPoints.size() + " double");
        
        // one line per vertex "x y z"
        for (Vertex v : packedMesh)
            ps.println(v.position.getX() + " " + v.position.getY() + " " + v.position.getZ());
        
        // neighborhood information "POLYGONS faces nbItems"
        ps.println("POLYGONS " + faces.size() + " " + 4 * faces.size());
        
        // one line per face "numVertices v1 v2 ... vn"
        for (Face f : packedMesh.faces)
            ps.println("3 " + f.v1 + " " + f.v2 + " " + f.v3);
    }
    
    /**
     * Returns the minimum distance between all contour points and the specified point
     * 
     * @param point
     * @return
     */
    public double getMinDistanceTo(Vector3D point)
    {
        return getMinDistanceTo(point, null);
    }
    
    /**
     * Returns the minimum distance between all contour points and the specified point.
     * 
     * @param point
     * @param closestPoint
     *            a Point3d object that will be filled with the closest point
     * @return
     */
    public double getMinDistanceTo(Vector3D point, Vector3D closestPoint)
    {
        double dist = Double.MAX_VALUE;
        
        for (Vertex v : this)
        {
            if (v == null) continue;
            
            double d = v.position.getL2Distance(point);
            
            if (d < dist)
            {
                dist = d;
                if (closestPoint != null) closestPoint.set(v.position);
            }
        }
        
        return dist;
    }
    
    /**
     * Returns the maximum distance between all contour points and the specified point
     * 
     * @param point
     * @return
     */
    public double getMaxDistanceTo(Vector3D point)
    {
        double dist = 0;
        
        for (Vertex v : controlPoints)
        {
            if (v == null) continue;
            double dc = v.position.getL2Distance(getCenter());
            if (dc > dist) dist = dc;
        }
        
        return dist;
    }
    
    public double getCurvature(Vector3D pt)
    {
        for (Vertex v : controlPoints)
        {
            if (v == null) continue;
            
            if (!v.position.equals(pt)) continue;
            
            Vector3D sum = new Vector3D();
            Vector3D diff = new Vector3D();
            for (Integer n : v.neighbors)
            {
                Vector3D neighbor = controlPoints.get(n).position;
                diff.set(neighbor).subtract(pt);
                sum.add(diff);
            }
            sum.scale(1.0 / v.neighbors.size());
            
            return sum.getNorm(2) * Math.signum(sum.dot(v.normal));
        }
        
        return 0;
    }
    
    public double getPerimeter()
    {
        double surface = 0;
        
        Vector3D v12 = new Vector3D();
        Vector3D v13 = new Vector3D();
        
        for (Face f : faces)
        {
            Vector3D v1 = controlPoints.get(f.v1).position;
            Vector3D v2 = controlPoints.get(f.v2).position;
            Vector3D v3 = controlPoints.get(f.v3).position;
            
            v12.set(v2).subtract(v1);
            v13.set(v3).subtract(v1);
            
            surface += v12.cross(v13).getNorm(2) * 0.5f;
        }
        
        return surface;
    }
    
    @Override
    public PolyhedronTopologyOperator getTopology()
    {
        return (PolyhedronTopologyOperator) super.getTopology();
    }
    
    public double getVolume(boolean algebraic)
    {
        double volume = 0;
        
        Vector3D v12 = new Vector3D();
        Vector3D v13 = new Vector3D();
        
        for (Face f : faces)
        {
            Vector3D v1 = controlPoints.get(f.v1).position;
            Vector3D v2 = controlPoints.get(f.v2).position;
            Vector3D v3 = controlPoints.get(f.v3).position;
            
            v12.set(v2).subtract(v1);
            v13.set(v3).subtract(v1);
            
            volume += v12.cross(v13).getNorm(2) * 0.5f * v12.getX() * (v1.getX() + v2.getX() + v3.getX());
        }
        
        return algebraic ? volume : Math.abs(volume);
    }
    
    public Vector3D getControlPointPosition(int index)
    {
        Vertex v = controlPoints.get(index);
        
        return (v == null) ? null : v.position;
    }
    
    /**
     * @param cp
     * @return 0 if the vertex is outside this mesh, otherwise a positive value measuring the
     *         penetration depth (in metrics unit)
     */
    public double isColliding(OldControlPoint<?> cp)
    {
        return isInside_IntersectionBased(cp);
    }
    
    public double isInside_IntersectionBased(OldControlPoint<?> vTest)
    {
        // FIXME this is one of the hottest spot in the entire plug-in
        
        double epsilon = 1.0e-12;
        
        // the given point belongs to another mesh
        // 1) trace a ray from that point outwards (away from my center)
        // 2) count the intersections with my boundary
        // 3) if the number is odd, the point is inside
        // 4) if the point is inside, measure the penetration
        
        // ray.negate(vTest.normal); // was supper buggy upon close contacts !
        ray.set(vTest.position).subtract(getCenter());
        
        double penetration = Double.MAX_VALUE;
        int crossCount = 0;
        
        double det, u, v, distance;
        
        for (Face f : faces)
        {
            Vector3D v1 = controlPoints.get(f.v1).position; // FIXME Null pointer ???
            Vector3D v2 = controlPoints.get(f.v2).position; // FIXME Null pointer ???
            Vector3D v3 = controlPoints.get(f.v3).position; // FIXME Null pointer ???
            
            edge1.set(v3).subtract(v1);
            edge2.set(v2).subtract(v1);
            
            vp.set(ray).cross(edge2);
            
            det = edge1.dot(vp);
            
            if (det < epsilon) continue;
            
            vt.set(vTest.position).subtract(v1);
            
            u = vt.dot(vp);
            
            if (u < 0 || u > det) continue;
            
            vq.set(vt).cross(edge1);
            
            v = ray.dot(vq);
            
            if (v < 0 || u + v > det) continue;
            
            distance = edge2.dot(vq) / det;
            
            if (distance < 0) continue;
            
            if (penetration > distance) penetration = distance;
            
            crossCount++;
        }
        
        return crossCount % 2 == 1 ? penetration : 0;
    }
    
    /**
     * @param sequence
     *            the data where means are computed
     * @param t
     *            the time point where means are computed
     * @param c
     *            the channel where means are computed
     * @param mask
     *            the (optional) labeled mask used to store all meshes (set to <code>null</code> if
     *            not needed)
     * @param multithreadservice
     *            the service to run on
     * @return
     * @throws ContourException
     *             if the mesh is flattening and should be removed
     */
    @Override
    public double computeIntensity(final Sequence sequence, int channel, final byte[][] byteMask, ExecutorService multiThreadService) throws ContourException
    {
        double inSum = 0, inCpt = 0;
        
        final int scanLine = sequence.getSizeX();
        final DataType dataType = sequence.getDataType_();
        
        Vector3D boxMin = new Vector3D();
        Vector3D boxMax = new Vector3D();
        
        boundingBox.getMinBounds(boxMin);
        boundingBox.getMaxBounds(boxMax);
        
        final double pixSizeX = sequence.getPixelSizeX();
        final double pixSizeY = sequence.getPixelSizeY();
        final double pixSizeZ = sequence.getPixelSizeZ();
        
        final double dataMin = sequence.getChannelMin(channel);
        final double dataMax = sequence.getChannelMax(channel);
        
        final int minX = Math.max(0, (int) Math.floor(boxMin.getX() / pixSizeX) - 1);
        final int minY = Math.max(0, (int) Math.floor(boxMin.getY() / pixSizeY) - 1);
        final int minZ = Math.max(0, (int) Math.floor(boxMin.getZ() / pixSizeZ) - 1);
        
        final int maxY = Math.min(sequence.getSizeY() - 1, (int) Math.ceil(boxMax.getY() / pixSizeY) + 1);
        final int maxZ = Math.min(sequence.getSizeZ() - 1, (int) Math.ceil(boxMax.getZ() / pixSizeZ) + 1);
        
        final double epsilon = 1.0e-12;
        
        // Ray direction is along X
        final Vector3D direction = new Vector3D(1, 0, 0);
        
        meanUpdateTasks.ensureCapacity(maxZ - minZ + 1);
        
        for (int k = minZ; k <= maxZ; k++)
        {
            final byte[] maskSlice = (byteMask != null) ? byteMask[k] : null;
            
            final int slice = k;
            
            final Object imageSlice = sequence.getDataXY(getT(), k, channel);
            
            meanUpdateTasks.add(new Callable<double[]>()
            {
                @Override
                public double[] call() throws Exception
                {
                    Vector3D edge1 = new Vector3D(), edge2 = new Vector3D();
                    Vector3D vp = new Vector3D(), vt = new Vector3D(), vq = new Vector3D();
                    
                    ArrayList<Integer> crossDistancesList = new ArrayList<Integer>(4);
                    
                    double localInSum = 0;
                    int localInCpt = 0;
                    
                    // Origin of the traced ray
                    double ox = minX * pixSizeX;
                    double oy = minY * pixSizeY;
                    double oz = slice * pixSizeZ;
                    
                    for (int j = minY; j < maxY; j++, oy += pixSizeY)
                    {
                        int lineOffset = j * scanLine;
                        
                        crossDistancesList.clear();
                        int crosses = 0;
                        
                        for (Face f : faces)
                        {
                            Vector3D v1 = controlPoints.get(f.v1).position;
                            Vector3D v2 = controlPoints.get(f.v2).position;
                            Vector3D v3 = controlPoints.get(f.v3).position;
                            
                            // if the ray doesn't intersect the triangle bounds, continue
                            if (oy < v1.getY() && oy < v2.getY() && oy < v3.getY()) continue;
                            if (oz < v1.getZ() && oz < v2.getZ() && oz < v3.getZ()) continue;
                            if (oy > v1.getY() && oy > v2.getY() && oy > v3.getY()) continue;
                            if (oz > v1.getZ() && oz > v2.getZ() && oz > v3.getZ()) continue;
                            
                            // compute the intersection
                            
                            edge1.set(v2).subtract(v1);
                            edge2.set(v3).subtract(v1);
                            
                            vp.set(direction).cross(edge2);
                            
                            double det = edge1.dot(vp);
                            
                            if (Math.abs(det) < epsilon) continue;
                            
                            double inv_det = 1.0 / det;
                            
                            // compute the ray-triangle intersection
                            
                            vt.set(ox, oy, oz).subtract(v1);
                            double u = vt.dot(vp) * inv_det;
                            if (u < 0 || u > 1.0) continue;
                            
                            vq.set(vt).cross(edge1);
                            double v = direction.dot(vq) * inv_det;
                            
                            // if the ray doesn't hit the triangle, move on
                            if (v < 0.0 || u + v > 1.0) continue;
                            
                            // from now, the ray does intersect
                            // => find the distance
                            
                            double distance = edge2.dot(vq) * inv_det;
                            
                            Integer distPx = minX + (int) Math.round(distance / pixSizeX);
                            
                            // if the distance is negative, not interesting
                            if (distPx < 0) continue;
                            
                            if (!crossDistancesList.contains(distPx))
                            {
                                crossDistancesList.add(distPx);
                                crosses++;
                            }
                            else
                            {
                                // if distPx already exists, the mesh "slices" the same voxel twice
                                // (rounding-off error gives the same distance for the 2 crosses)
                                // => consider the voxel as "not crossed"
                                crossDistancesList.remove(distPx);
                                crosses--;
                            }
                        }
                        
                        // ignore situations with no intersections (the ray does not cross the mesh)
                        // or 1 cross (the ray starts from the edge of the mesh, and only the
                        // exiting intersection was recorded
                        if (crosses < 2) continue;
                        
                        // sort the cross distances in ascending order
                        Collections.sort(crossDistancesList);
                        
                        for (int cross = 0; cross < crosses;)
                        {
                            int in = crossDistancesList.get(cross++);
                            int out = crossDistancesList.get(cross++);
                            
                            for (int i = in; i < out; i++)
                            {
                                localInCpt++;
                                int offset = i + lineOffset;
                                // accumulate the image data (rescaled to [0-1] on the fly)
                                localInSum += (Array1DUtil.getValue(imageSlice, offset, dataType) - dataMin) / (dataMax - dataMin);
                                if (maskSlice != null) maskSlice[offset] = 1;
                            }
                        }
                    }
                    
                    return (localInCpt == 0) ? new double[2] : new double[] { localInSum, localInCpt };
                }
            });
        }
        
        try
        {
            for (Future<double[]> localMean : multiThreadService.invokeAll(meanUpdateTasks))
            {
                double[] sum_cpt = localMean.get();
                inSum += sum_cpt[0];
                inCpt += sum_cpt[1];
            }
        }
        catch (InterruptedException e)
        {
            // Restore the interrupted flag
            Thread.currentThread().interrupt();
        }
        catch (ExecutionException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            meanUpdateTasks.clear();
        }
        
        if (inCpt == 0)
        {
            // no voxel inside the mesh => mesh is becoming extremely flat
            throw new ContourException(this, "Flat mesh (probably on the volume edge)");
        }
        return inSum / inCpt;
    }
    
    @Override
    public void updateNormals()
    {
        Vector3D v31 = new Vector3D();
        Vector3D v12 = new Vector3D();
        Vector3D v23 = new Vector3D();
        
        Vector3D normal = new Vector3D();
        
        for (Face f : faces)
        {
            // Accumulate face normals in each vertex
            
            Vertex v1 = controlPoints.get(f.v1);
            Vertex v2 = controlPoints.get(f.v2);
            Vertex v3 = controlPoints.get(f.v3);
            
            v31.set(v1.position).subtract(v3.position);
            v12.set(v2.position).subtract(v1.position);
            v23.set(v3.position).subtract(v2.position);
            
            // normal at v1 = [v1 v2] ^ [v1 v3] = [v3 v1] ^ [v1 v2]
            v1.normal.add(normal.set(v31).cross(v12));
            // v1.normal.x += v31.y * v12.z - v31.z * v12.y;
            // v1.normal.y += v31.z * v12.x - v31.x * v12.z;
            // v1.normal.z += v31.x * v12.y - v31.y * v12.x;
            
            // normal at v2 = [v2 v3] ^ [v2 v1] = [v1 v2] ^ [v2 v3]
            v2.normal.add(normal.set(v12).cross(v23));
            // v2.normal.x += v12.y * v23.z - v12.z * v23.y;
            // v2.normal.y += v12.z * v23.x - v12.x * v23.z;
            // v2.normal.z += v12.x * v23.y - v12.y * v23.x;
            
            // normal at v3 = [v3 v1] ^ [v3 v2] = [v2 v3] ^ [v3 v1]
            v3.normal.add(normal.set(v23).cross(v31));
            // v3.normal.x += v23.y * v31.z - v23.z * v31.y;
            // v3.normal.y += v23.z * v31.x - v23.x * v31.z;
            // v3.normal.z += v23.x * v31.y - v23.y * v31.x;
        }
        
        // Normalize the accumulated normals
        for (Vertex v : controlPoints)
            if (v != null) v.normal.normalize(2);
    }
    
    public class PolyhedronTopologyOperator implements TopologyOperator
    {
        private final ArrayList<Vertex> tmpVertices = new ArrayList<Vertex>();
        private final ArrayList<Face>   tmpFaces    = new ArrayList<Face>();
        
        public boolean                  updating    = false;
        
        public boolean                  lock        = false;
        
        public boolean beginUpdate(boolean returnNow)
        {
            if ((this.lock) && (returnNow))
            {
                return false;
            }
            this.updating = true;
            
            synchronized (this)
            {
                copy(controlPoints, this.tmpVertices);
                copy(faces, this.tmpFaces);
            }
            
            return true;
        }
        
        public void endUpdate()
        {
            synchronized (this)
            {
                copy(this.tmpVertices, (ArrayList<Vertex>) controlPoints);
                copy(this.tmpFaces, (ArrayList<Face>) faces);
            }
            
            this.updating = false;
        }
        
        private <T> void copy(List<? super T> src, ArrayList<? extends T> dest)
        {
            if (dest.size() > src.size()) dest.clear();
            dest.ensureCapacity(src.size());
            Collections.copy(src, dest);
        }
        
        /**
         * Refine the mesh by doubling its resolution
         * 
         * @throws ContourException
         */
        public void refine() throws ContourException
        {
            beginUpdate(false);
            
            {
                tmpFaces.clear();
                tmpFaces.ensureCapacity(faces.size() * 4);
                
                for (Vertex v : tmpVertices)
                    if (v != null) v.neighbors.clear();
                
                for (Face f : tmpFaces)
                {
                    int centerv1v2 = addVertexBetween(f.v1, f.v2);
                    
                    int centerv2v3 = addVertexBetween(f.v2, f.v3);
                    
                    int centerv3v1 = addVertexBetween(f.v3, f.v1);
                    
                    addFace(f.v1, centerv1v2, centerv3v1);
                    addFace(centerv1v2, f.v2, centerv2v3);
                    addFace(centerv2v3, f.v3, centerv3v1);
                    addFace(centerv1v2, centerv2v3, centerv3v1);
                }
                
                resolution.setValue(resolution.getValue() / 2);
            }
            
            endUpdate();
        }
        
        /**
         * Multiplies all vertex coordinates by the given factor
         * 
         * @param factor
         */
        public void scale(double factor)
        {
            for (Vertex v : tmpVertices)
                v.position.scale(factor);
        }
        
        @Override
        public void reSample() throws ContourException, MeshSplittingException
        {
            while (!beginUpdate(true))
                ThreadUtil.sleep(10);
            
            {
                updating = true;
                double minLength = resolution.getValue() * MIN_RESAMPLE_FACTOR;
                double maxLength = resolution.getValue() * MAX_RESAMPLE_FACTOR;
                
                // if there are 2 faces only in the mesh, it should be destroyed
                
                if (tmpFaces.size() == 2)
                {
                    throw new ContourException(Polyhedron.this, "The mesh is now empty");
                }
                
                boolean change = true;
                
                int cpt = -1;
                
                while (change)
                {
                    cpt++;
                    
                    // we are looking for 2 faces f1 = a-b-c1 and f2 = b-a-c2
                    // such that they share an edge a-b that is either
                    // - lower than the low-threshold (Resolution * min)
                    // or
                    // - higher than the high-threshold (Resolution * max)
                    
                    change = false;
                    
                    for (int i = 0; i < tmpFaces.size(); i++)
                    {
                        boolean split = false, merge = false;
                        
                        Face f1 = tmpFaces.get(i);
                        Integer v1 = 0, v2 = 0, f1v3 = 0;
                        
                        // Look first for f1 = a-b-c1
                        
                        Integer[] f1v123 = { f1.v1, f1.v2, f1.v3 };
                        Integer[] f1v231 = { f1.v2, f1.v3, f1.v1 };
                        Integer[] f1v312 = { f1.v3, f1.v1, f1.v2 };
                        
                        for (int v = 0; v < 3; v++)
                        {
                            v1 = f1v123[v];
                            v2 = f1v231[v];
                            f1v3 = f1v312[v];
                            
                            double edgeLength = tmpVertices.get(v1).position.getL2Distance(tmpVertices.get(v2).position);
                            
                            if (edgeLength < minLength)
                            {
                                merge = true;
                                break;
                            }
                            
                            if (edgeLength > maxLength)
                            {
                                split = true;
                                break;
                            }
                        }
                        
                        if (split == merge)
                        {
                            // they are necessarily both false
                            continue; // to the next face
                        }
                        
                        // If the code runs here, f1 has been found,
                        // so now we must find f2 and c2
                        change = true;
                        Face f2 = null;
                        Integer f2v3 = -1;
                        
                        for (int j = i + 1; j < tmpFaces.size(); j++)
                        {
                            f2 = tmpFaces.get(j);
                            
                            // check if f2 contains edge a-b in any way possible
                            if (v1.compareTo(f2.v1) == 0 && v2.compareTo(f2.v3) == 0)
                            {
                                f2v3 = f2.v2;
                                break;
                            }
                            else if (v1.compareTo(f2.v2) == 0 && v2.compareTo(f2.v1) == 0)
                            {
                                f2v3 = f2.v3;
                                break;
                            }
                            else if (v1.compareTo(f2.v3) == 0 && v2.compareTo(f2.v2) == 0)
                            {
                                f2v3 = f2.v1;
                                break;
                            }
                        }
                        
                        if (f2v3.compareTo(0) < 0)
                        {
                            // here, the mesh is inconsistent: vA and vB are linked in a
                            // unique face (it should be 2)
                            // so remove the face and the link between vA and vB
                            
                            System.err.println("Problem in face " + i + ":");
                            System.err.print("  " + f1.v1.intValue() + " : ");
                            for (Integer nn : tmpVertices.get(v1).neighbors)
                                System.err.print(nn.intValue() + "  ");
                            System.err.println();
                            System.err.print("  " + f1.v2.intValue() + " : ");
                            for (Integer nn : tmpVertices.get(v2).neighbors)
                                System.err.print(nn.intValue() + "  ");
                            System.err.println();
                            System.err.print("  " + f1.v3.intValue() + " : ");
                            for (Integer nn : tmpVertices.get(f1.v3).neighbors)
                                System.err.print(nn.intValue() + "  ");
                            System.err.println();
                            
                            tmpFaces.remove(f1);
                            
                            // pointsTMP.get(v1).neighbors.remove(v2);
                            // pointsTMP.get(v2).neighbors.remove(v1);
                        }
                        else if (merge)
                        {
                            merge(f1, f2, v1, v2, f1v3, f2v3, getMinimumVolume());
                        }
                        else
                        // split
                        {
                            // check if the edge should be split or inverted
                            if (!tmpVertices.get(f1v3).neighbors.contains(f2v3) && tmpVertices.get(f1v3).position.getL2Distance(tmpVertices.get(f2v3).position) < maxLength)
                            {
                                // invert the edge
                                
                                tmpFaces.remove(f1);
                                tmpFaces.remove(f2);
                                
                                // the two controlPoints must not be neighbors anymore
                                tmpVertices.get(v1).neighbors.remove(v2);
                                tmpVertices.get(v2).neighbors.remove(v1);
                                
                                // create the two new faces
                                addFace(f1v3, v1, f2v3);
                                addFace(f2v3, v2, f1v3);
                            }
                            else
                            {
                                // split the edge
                                
                                tmpFaces.remove(f1);
                                tmpFaces.remove(f2);
                                
                                // create the vertex in the middle of the edge and add its new
                                // neighbors
                                Integer c = addVertexBetween(v1, v2);
                                if (c < 0) c = -(c + 1);
                                
                                // the two controlPoints must not be neighbors anymore
                                tmpVertices.get(v1).neighbors.remove(v2);
                                tmpVertices.get(v2).neighbors.remove(v1);
                                
                                // create 2 new faces per old face
                                addFace(v1, c, f1v3);
                                addFace(f1v3, c, v2);
                                addFace(v1, f2v3, c);
                                addFace(c, f2v3, v2);
                                
                            }
                        }
                        
                        break;
                    }
                    
                    // prevent infinite loop
                    if (cpt > tmpVertices.size()) change = false;
                }
            }
            endUpdate();
        }
        
        /**
         * Merges two points of an edge (supposedly too close) by replacing the edge and its two
         * corresponding faces by a single point in its center
         * 
         * @param f1
         *            the first face containing the edge
         * @param f2
         *            the second face containing the edge
         * @param v1
         *            the first vertex of the edge
         * @param v2
         *            the second vertex of the edge
         * @param f1v3
         *            the third vertex of f1
         * @param f2v3
         *            the third vertex of f2
         * @param minVolume
         *            the minimum volume of a mesh (used in case a mesh division is detected)
         * @throws ContourSplittingException
         *             if the merge operation leads to a mesh splitting
         */
        private void merge(Face f1, Face f2, Integer v1, Integer v2, Integer f1v3, Integer f2v3, double minVolume) throws MeshSplittingException
        {
            // Check if the edge to merge is the base of a tetrahedron
            // if so, delete the whole tetrahedron
            if (tmpVertices.get(f1v3).neighbors.size() == 3)
            {
                deleteTetrahedron(f1v3, v1, v2);
                return;
            }
            if (tmpVertices.get(f2v3).neighbors.size() == 3)
            {
                deleteTetrahedron(f2v3, v2, v1);
                return;
            }
            
            Vertex vx1 = tmpVertices.get(v1);
            Vertex vx2 = tmpVertices.get(v2);
            
            for (Integer n : vx1.neighbors)
                if (vx2.neighbors.contains(n) && n.compareTo(f1v3) != 0 && n.compareTo(f2v3) != 0)
                {
                    splitContourAtVertices(v1, v2, n, minVolume);
                    return;
                }
            
            // Here, the normal merge operation can be implemented
            
            // remove the 2 faces
            tmpFaces.remove(f1);
            tmpFaces.remove(f2);
            
            // move v1 to the middle of v1-v2
            // vx1.position.interpolate(vx2.position, 0.5);
            vx1.position.add(vx2.position).scale(0.5);
            
            // remove v2 from its neighborhood...
            for (Integer n : vx2.neighbors)
            {
                Vertex vxn = tmpVertices.get(n);
                if (vxn == null) continue;
                vxn.neighbors.remove(v2);
                
                // ...and add v2's neighbors to v1
                // except for f1v3 and f2v3 and ... v1 !
                if (n.compareTo(f1v3) != 0 && n.compareTo(f2v3) != 0 && n.compareTo(v1) != 0)
                {
                    vx1.neighbors.add(n);
                    vxn.neighbors.add(v1);
                }
            }
            
            // all the faces pointing to v2 must now point to v1
            for (Face f : tmpFaces)
            {
                if (f.v1.compareTo(v2) == 0)
                {
                    f.v1 = v1;
                }
                else if (f.v2.compareTo(v2) == 0)
                {
                    f.v2 = v1;
                }
                else if (f.v3.compareTo(v2) == 0)
                {
                    f.v3 = v1;
                }
            }
            
            // delete everything
            tmpVertices.set(v2, null);
        }
        
        /**
         * If it doens't exist, adds a new vertex in the center of the specified controlPoints to
         * the vertex list
         * 
         * @param vIndex1
         *            the first vertex index
         * @param vIndex2
         *            the second vertex index
         * @return A signed integer i defined as follows: <br>
         *         i>=0 : vertex is new and stored at position i <br>
         *         i<0 : vertex already existed at position -(i+1)
         */
        private Integer addVertexBetween(Integer vIndex1, Integer vIndex2)
        {
            Vector3D newPosition = new Vector3D(tmpVertices.get(vIndex1).position);
            newPosition.add(tmpVertices.get(vIndex2).position).scale(0.5);
            
            return addVertex(newPosition);
        }
        
        /**
         * If it doens't exist, adds a new vertex in the center of the specified controlPoints to
         * the vertex list
         * 
         * @param point
         *            the point to add
         * @return A signed integer i defined as follows: <br>
         *         i>=0 : vertex is new and stored at position i <br>
         *         i<0 : vertex already existed at position -(i+1)
         */
        public Integer addVertex(Vector3D point)
        {
            Integer index, nullIndex = -1;
            
            for (index = 0; index < tmpVertices.size(); index++)
            {
                Vertex v = tmpVertices.get(index);
                
                if (v == null)
                {
                    // The current position in the vertex list is null.
                    // To avoid growing the data array, this position
                    // can be reused to store a new vertex if needed
                    nullIndex = index;
                    continue;
                }
                
                if (v.position.getL2DistanceSquared(point) < resolution.getValue() * 0.00001) return -index - 1;
            }
            
            // if code runs until here, the vertex must be created
            Vertex v = new Vertex(new Vector3D(point));
            
            // if there is a free spot in the ArrayList, use it
            if (nullIndex >= 0)
            {
                index = nullIndex;
                tmpVertices.set(index, v);
            }
            else
            {
                tmpVertices.add(v);
            }
            
            return index;
        }
        
        public void addFace(Integer v1, Integer v2, Integer v3) throws ContourException
        {
            if (v1.compareTo(v2) == 0 || v1.compareTo(v3) == 0 || v2.compareTo(v3) == 0)
                throw new ContourException(Polyhedron.this, "wrong face declared: (" + v1 + "," + v2 + "," + v3 + ")");
            
            if (v1 < 0) v1 = -v1 - 1;
            if (v2 < 0) v2 = -v2 - 1;
            if (v3 < 0) v3 = -v3 - 1;
            
            {
                Vertex v = tmpVertices.get(v1);
                if (!v.neighbors.contains(v2)) v.neighbors.add(v2);
                if (!v.neighbors.contains(v3)) v.neighbors.add(v3);
            }
            {
                Vertex v = tmpVertices.get(v2);
                if (!v.neighbors.contains(v1)) v.neighbors.add(v1);
                if (!v.neighbors.contains(v3)) v.neighbors.add(v3);
            }
            {
                Vertex v = tmpVertices.get(v3);
                if (!v.neighbors.contains(v1)) v.neighbors.add(v1);
                if (!v.neighbors.contains(v2)) v.neighbors.add(v2);
            }
            
            tmpFaces.add(new Face(v1, v2, v3));
        }
        
        /**
         * Splits the current contour using the 'cutting' face defined by the given controlPoints. <br>
         * 
         * <pre>
         * How this works:
         *  - separate all controlPoints on each side of the cutting face (without considering the controlPoints of the cutting face), 
         *  - separate all faces touching at least one vertex of each group (will include faces touching the cutting face),
         *  - create a contour with each group of controlPoints and faces,
         *  - add the cutting face and its controlPoints to each created contour
         * </pre>
         * 
         * @param v1
         * @param v2
         * @param v3
         * @param minVolume
         * @throws ContourSplittingException
         */
        private void splitContourAtVertices(Integer v1, Integer v2, Integer v3, double minVolume) throws MeshSplittingException
        {
            Polyhedron[] children = new Polyhedron[2];
            
            ArrayList<Integer> visitedIndexes = new ArrayList<Integer>(tmpVertices.size());
            visitedIndexes.add(v1);
            visitedIndexes.add(v2);
            visitedIndexes.add(v3);
            
            int seed;
            
            for (int child = 0; child < 2; child++)
            {
                // pick any non-null and non-visited vertex as seed
                for (seed = 0; seed < tmpVertices.size(); seed++)
                    if (tmpVertices.get(seed) != null && !visitedIndexes.contains(seed)) break;
                
                if (seed == tmpVertices.size())
                {
                    System.err.println("Mesh splitting error (pass " + (child + 1) + "): no valid seed found");
                    throw new MeshSplittingException(Polyhedron.this, new Polyhedron[0]);
                }
                
                ArrayList<Face> newFaces = new ArrayList<Face>();
                ArrayList<Vertex> newPoints = new ArrayList<Vertex>(tmpVertices.size());
                for (int i = 0; i < tmpVertices.size(); i++)
                    newPoints.add(null);
                
                extractVertices(seed, visitedIndexes, tmpVertices, newPoints);
                extractFaces(newPoints, tmpFaces, newFaces);
                
                // Add the controlPoints of the cutting face
                for (Integer v : new Integer[] { v1, v2, v3 })
                {
                    Vertex vx = tmpVertices.get(v);
                    
                    if (vx == null) System.err.println(v.intValue() + " is null");
                    
                    // create a clone for each vertex (position and neighbors)
                    Vertex newV = new Vertex(tmpVertices.get(v));
                    
                    // check the neighborhood to remove the neighbors that belong to
                    // the other mesh
                    // (these neighbors will point to null in the current point
                    // list)
                    for (int i = 0; i < newV.neighbors.size(); i++)
                    {
                        Integer n = newV.neighbors.get(i);
                        if (n.compareTo(v1) != 0 && n.compareTo(v2) != 0 && n.compareTo(v3) != 0 && newPoints.get(n) == null)
                        {
                            newV.neighbors.remove(n);
                            i--;
                        }
                    }
                    newPoints.set(v, newV);
                }
                
                for (Face f : newFaces)
                    if (f.contains(v1) && f.contains(v2))
                    {
                        // if the edge v1-v2 appears counter-clockwisely in f,
                        // the new face must be clockwise and vice-versa
                        newFaces.add(f.isCounterClockwise(v1, v2) ? new Face(v1, v3, v2) : new Face(v1, v2, v3));
                        break;
                    }
                
                Polyhedron newContour = new Polyhedron(newPoints, newFaces, resolution, timeStep);
                
                newContour.setT(getT());
                
                if (newContour.getVolume(false) >= minVolume) children[child] = newContour;
            }
            
            // no children?
            if (children[0] == null && children[1] == null) throw new MeshSplittingException(Polyhedron.this, new Polyhedron[0]);
            
            // 2 children?
            if (children[0] != null && children[1] != null) throw new MeshSplittingException(Polyhedron.this, children);
            
            // single child
            
            Polyhedron onlyChild = (children[0] != null) ? children[0] : children[1];
            
            copy(onlyChild.controlPoints, tmpVertices);
            copy(onlyChild.faces, tmpFaces);
        }
        
        private void extractVertices(Integer seedIndex, ArrayList<Integer> visitedIndices, List<Vertex> oldPoints, ArrayList<Vertex> newPoints)
        {
            Stack<Integer> seeds = new Stack<Integer>();
            seeds.add(seedIndex);
            
            while (!seeds.isEmpty())
            {
                Integer seed = seeds.pop();
                
                if (visitedIndices.contains(seed)) continue;
                
                visitedIndices.add(seed);
                Vertex v = oldPoints.get(seed);
                newPoints.set(seed, v);
                
                for (int i = 0; i < v.neighbors.size(); i++)
                {
                    Integer n = v.neighbors.get(i);
                    if (oldPoints.get(n) == null)
                    {
                        v.neighbors.remove(n);
                        i--;
                        continue;
                    }
                    seeds.push(n);
                }
            }
        }
        
        private void extractFaces(List<Vertex> pointsList, List<Face> sourceFacesList, List<Face> targetFacesList)
        {
            for (int i = 0; i < sourceFacesList.size(); i++)
            {
                Face f = sourceFacesList.get(i);
                
                if (pointsList.get(f.v1) != null || pointsList.get(f.v2) != null || pointsList.get(f.v3) != null)
                {
                    targetFacesList.add(f);
                    sourceFacesList.remove(i--);
                }
            }
        }
        
        /**
         * Deletes a tetrahedron from the mesh, and fill the hole with a new face
         * 
         * @param topv
         *            the vertex at the top of the tetrahedron
         * @param v1
         *            one of the three controlPoints at the base of the tetrahedron
         * @param v2
         *            another of the controlPoints at the base of the tetrahedron
         */
        private void deleteTetrahedron(Integer topv, Integer v1, Integer v2)
        {
            // check for mesh inconsistency
            if (v1.compareTo(v2) == 0) throw new IllegalArgumentException("Invalid topology detected in deleteTetrahedron()");
            
            // find the third bottom vertex
            Integer v3 = -1;
            for (Integer n : tmpVertices.get(topv).neighbors)
            {
                if (n.compareTo(v1) == 0 || n.compareTo(v2) == 0) continue;
                v3 = n;
                break;
            }
            
            // remove the top vertex from the neighborhood
            tmpVertices.get(v1).neighbors.remove(topv);
            tmpVertices.get(v2).neighbors.remove(topv);
            tmpVertices.get(v3).neighbors.remove(topv);
            
            // delete the top vertex
            tmpVertices.set(topv, null);
            
            // find the three faces and delete them
            for (int i = 0; i < tmpFaces.size(); i++)
            {
                Face f = tmpFaces.get(i);
                if (f.v1.compareTo(topv) == 0 || f.v2.compareTo(topv) == 0 || f.v3.compareTo(topv) == 0) tmpFaces.remove(i--);
            }
            
            // create the new face to replace the tetrahedron base
            tmpFaces.add(new Face(v1, v2, v3));
        }
        
        /**
         * Extract the individual meshes bundled into this mesh structure. If the current topology
         * only contains a single mesh, this mesh is returned. This method can be used if the result
         * of an initialization yields a unique mesh structure containing multiple non-connected
         * meshes representing different objects
         * 
         * @return
         */
        public Collection<Polyhedron> extractMeshes()
        {
            ArrayList<Polyhedron> meshes = new ArrayList<Polyhedron>();
            
            ArrayList<Integer> visitedIndexes = new ArrayList<Integer>(controlPoints.size());
            
            int seed;
            
            while (true)
            {
                for (seed = 0; seed < controlPoints.size(); seed++)
                    if (controlPoints.get(seed) != null && !visitedIndexes.contains(seed)) break;
                
                if (seed == controlPoints.size()) break;
                
                ArrayList<Face> newFaces = new ArrayList<Face>();
                ArrayList<Vertex> newPoints = new ArrayList<Vertex>(controlPoints.size());
                for (int i = 0; i < controlPoints.size(); i++)
                    newPoints.add(null);
                
                extractVertices(seed, visitedIndexes, controlPoints, newPoints);
                extractFaces(newPoints, faces, newFaces);
                
                meshes.add(new Polyhedron(newPoints, newFaces, resolution, timeStep));
            }
            
            return meshes;
        }
    }
    
    @Override
    public List<Vertex> getNeighbors(Vertex controlPoint)
    {
        List<Vertex> neighbors = new ArrayList<Vertex>(controlPoint.neighbors.size());
        
        for (Integer n : controlPoint.neighbors)
            neighbors.add(controlPoints.get(n));
        
        return neighbors;
    }
}
