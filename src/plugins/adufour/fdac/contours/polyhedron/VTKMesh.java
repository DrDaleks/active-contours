package plugins.adufour.fdac.contours.polyhedron;

import icy.vtk.VtkUtil;
import vtk.vtkActor;
import vtk.vtkDoubleArray;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkRenderer;

public class VTKMesh
{
    public final Polyhedron         mesh;
    private final vtkDoubleArray    vtkVerticesCoords = new vtkDoubleArray();
    private final vtkPoints         vtkVerticesInfo   = new vtkPoints();
    
    public final vtkPolyData        polyData          = new vtkPolyData();
    private final vtkPolyDataMapper polyDataMapper    = new vtkPolyDataMapper();
    public final vtkActor           actor             = new vtkActor();
    private vtkRenderer             renderer;
    
    public VTKMesh(Polyhedron mesh)
    {
        this.mesh = mesh;
        
        this.vtkVerticesCoords.SetNumberOfComponents(3);
        this.vtkVerticesInfo.SetData(this.vtkVerticesCoords);
        this.polyData.SetPoints(this.vtkVerticesInfo);
        
        this.polyDataMapper.SetInputData(this.polyData);
        
        this.actor.SetMapper(this.polyDataMapper);
    }
    
    public void update()
    {
        if (mesh.getTopology().updating) return;
        
        int nFaces = this.mesh.faces.size();
        
        if (nFaces == 0) return;
        
        mesh.getTopology().lock = true;
        
        double[] vertices = new double[this.mesh.getNbPoints() * 3];
        
        int cIndex = 0;
        for (Vertex vertex : this.mesh)
        {
            if (vertex == null)
            {
                cIndex += 3;
            }
            else
            {
                vertices[(cIndex++)] = vertex.position.getX();
                vertices[(cIndex++)] = vertex.position.getY();
                vertices[(cIndex++)] = vertex.position.getY();
            }
        }
        this.vtkVerticesCoords.SetJavaArray(vertices);
        
        int[] faces = new int[nFaces * 4];
        
        int vIndex = 0;
        for (Face face : this.mesh.faces)
        {
            faces[(vIndex++)] = 3;
            faces[(vIndex++)] = face.v1.intValue();
            faces[(vIndex++)] = face.v2.intValue();
            faces[(vIndex++)] = face.v3.intValue();
        }
        
        this.polyData.SetPolys(VtkUtil.getCells(nFaces, faces));
        
        mesh.getTopology().lock = false;
    }
    
    public void clean()
    {
        if ((this.renderer != null) && (this.actor != null)) this.renderer.RemoveActor(this.actor);
    }
}