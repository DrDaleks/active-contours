package plugins.adufour.fdac.contours;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

import icy.roi.ROI;
import icy.sequence.Sequence;
import plugins.adufour.fdac.geom.Vector3D;
import plugins.adufour.vars.lang.VarDouble;

public abstract class Contour3D<CP extends OldControlPoint<Vector3D>> extends Contour<CP>
{
    private int t;
    
    /**
     * Constructs a new 3D active contour
     * 
     * @param timeStep
     *            the evolution time step
     * @param resolution
     *            the approximate distance in pixel units between 2 contour vertices. Note that any
     *            changes to this variable during deformation will immediately affect the contour
     */
    public Contour3D(VarDouble resolution, VarDouble timeStep)
    {
        this(resolution, timeStep, 0);
    }
    
    /**
     * Constructs a new 3D active contour with predefined number of points
     * 
     * @param timeStep
     *            the evolution time step
     * @param resolution
     *            the approximate distance in pixel units between 2 contour vertices. Note that any
     *            changes to this variable during deformation will immediately affect the contour
     * @param nbPoints
     *            the initial number of points (may vary later on)
     */
    public Contour3D(VarDouble resolution, VarDouble timeStep, int nbPoints)
    {
        this(resolution, timeStep, new ArrayList<CP>(nbPoints));
    }
    
    /**
     * Constructs a new 3D active contour with predefined set of control points
     * 
     * @param timeStep
     *            the evolution time step
     * @param resolution
     *            the approximate distance in pixel units between 2 contour vertices. Note that any
     *            changes to this variable during deformation will immediately affect the contour
     * @param controlPoints
     *            the initial control points (the list is not copied)
     */
    public Contour3D(VarDouble resolution, VarDouble timeStep, List<CP> controlPoints)
    {
        super(3, resolution, timeStep, controlPoints);
    }
    
    @Override
    public double computeIntensity(Sequence sequence, int channel, Object byteMask, ExecutorService multiThreadService) throws ContourException
    {
        return computeIntensity(sequence, channel, (byte[][]) byteMask, multiThreadService);
    }
    
    /**
     * @param sequence
     *            the data where means are computed
     * @param t
     *            the time point where means are computed
     * @param c
     *            the channel where means are computed
     * @param mask
     *            the (optional) labeled mask used to store all meshes (set to <code>null</code> if
     *            not needed). The mask a 2D byte array shaped as [Z][XY]
     * @param multithreadservice
     *            the service to run on
     * @return
     * @throws ContourException
     *             if the mesh is flattening and should be removed
     */
    public abstract double computeIntensity(final Sequence sequence, int channel, final byte[][] mask, ExecutorService multiThreadService) throws ContourException;
    
    /**
     * @return the T position of this 2D contour
     */
    public int getT()
    {
        return t;
    }
    
    /**
     * Sets the T position of this 2D contour
     * 
     * @param t
     */
    public void setT(int t)
    {
        this.t = t;
    }
    
    @Override
    protected boolean isInside(ROI field, CP cp)
    {
        return field.contains(cp.position.getX(), cp.position.getY(), cp.position.getZ(), -1, -1);
    }
}
