package plugins.adufour.fdac.contours;

/**
 * Top-level class for all exceptions occurring in the active meshes framework
 * 
 * @author Alexandre Dufour
 */
public class ContourException extends RuntimeException
{
    private static final long serialVersionUID = 1L;
    
    public final Contour<?>   contour;
    
    /**
     * Creates a new exception occurring on the specified contour
     * 
     * @param mesh
     * @param message
     */
    public ContourException(Contour<?> contour, String message)
    {
        super(message);
        this.contour = contour;
    }
}
