package plugins.adufour.fdac.contours;

public interface TopologyOperator
{
    public final double MIN_RESAMPLE_FACTOR = 0.7;
    
    public final double MAX_RESAMPLE_FACTOR = 1.4;
    
    public abstract void reSample() throws TopologyException;
}
