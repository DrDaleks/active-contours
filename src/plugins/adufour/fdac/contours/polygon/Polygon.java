package plugins.adufour.fdac.contours.polygon;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Path2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;

import icy.canvas.IcyCanvas;
import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;
import plugins.adufour.fdac.contours.Contour;
import plugins.adufour.fdac.contours.Contour2D;
import plugins.adufour.fdac.contours.ContourException;
import plugins.adufour.fdac.contours.OldControlPoint;
import plugins.adufour.fdac.contours.TopologyException;
import plugins.adufour.fdac.contours.TopologyOperator;
import plugins.adufour.fdac.geom.Vector2D;
import plugins.adufour.vars.lang.VarDouble;

/**
 * Specialized implementation of a {@link Contour} representing a 2D polygon. The list of control
 * points is considered to be ordered, such that consecutive control points on the underlying list
 * are also neighbors in the actual contour
 * 
 * @author Alexandre Dufour
 */
public class Polygon extends Contour2D<OldControlPoint<Vector2D>>
{
    private Path2D.Double path;
    
    public Polygon(VarDouble resolution, VarDouble timeStep)
    {
        super(resolution, timeStep);
    }
    
    public Polygon(VarDouble resolution, VarDouble timeStep, List<OldControlPoint<Vector2D>> controlPoints)
    {
        super(resolution, timeStep, controlPoints);
    }
    
    @Override
    public Polygon clone()
    {
        List<OldControlPoint<Vector2D>> pointsCopy = new ArrayList<OldControlPoint<Vector2D>>(controlPoints.size());
        
        for (OldControlPoint<Vector2D> cp : controlPoints)
            pointsCopy.add(new OldControlPoint<Vector2D>(cp));
        
        return new Polygon(resolution, timeStep, pointsCopy);
    }
    
    @Override
    protected TopologyOperator createTopologyOperator()
    {
        return new PolygonTopologyOperator();
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public List<OldControlPoint<Vector2D>> getNeighbors(OldControlPoint<Vector2D> controlPoint)
    {
        int i = controlPoints.indexOf(controlPoint);
        int n1 = (i - 1) % controlPoints.size();
        int n2 = (i + 1) % controlPoints.size();
        return Arrays.asList(controlPoints.get(n1), controlPoints.get(n2));
    }
    
    @Override
    public double getPerimeter()
    {
        double perimeter = 0;
        
        synchronized (controlPoints)
        {
            int n = controlPoints.size();
            
            // all points but the last
            for (int i = 0; i < n - 1; i++)
            {
                Vector2D p1 = controlPoints.get(i).position;
                Vector2D p2 = controlPoints.get(i + 1).position;
                perimeter += p1.getL2Distance(p2);
            }
            
            // last point
            {
                Vector2D p1 = controlPoints.get(n - 1).position;
                Vector2D p2 = controlPoints.get(0).position;
                perimeter += p1.getL2Distance(p2);
            }
        }
        
        return perimeter;
    }
    
    @Override
    public double getVolume(boolean algebraic)
    {
        double volume = 0;
        
        synchronized (controlPoints)
        {
            int n = controlPoints.size();
            
            // all points but the last
            for (int i = 0; i < n - 1; i++)
            {
                Vector2D p1 = controlPoints.get(i).position;
                Vector2D p2 = controlPoints.get(i + 1).position;
                volume += (p2.getX() * p1.getY() - p1.getX() * p2.getY()) * 0.5;
            }
            
            // last point
            {
                Vector2D p1 = controlPoints.get(n - 1).position;
                Vector2D p2 = controlPoints.get(0).position;
                volume += (p2.getX() * p1.getY() - p1.getX() * p2.getY()) * 0.5;
            }
        }
        
        return algebraic ? volume : Math.abs(volume);
    }
    
    @Override
    public double computeIntensity(Sequence sequence, int channel, byte[] mask, ExecutorService multiThreadService) throws ContourException
    {
        final int sizeX = sequence.getSizeX();
        final int sizeY = sequence.getSizeY();
        final DataType dataType = sequence.getDataType_();
        
        final IcyBufferedImage image = sequence.getImage(getT(), getZ());
        final Object data = image.getDataXY(channel);
        final double min = image.getChannelMin(channel);
        final double max = image.getChannelMax(channel);
        final double max_min = max - min;
        
        // add the contour to the global mask for background mean measuring
        new IcyBufferedImage(sizeX, sizeY, mask).createGraphics().fill(path);
        
        // compute the interior mean intensity
        double inSum = 0, inCpt = 0;
        Rectangle bounds = path.getBounds();
        
        int minX = Math.max(bounds.x, 0);
        int maxX = Math.min(bounds.x + bounds.width, sizeX);
        int minY = Math.max(bounds.y, 0);
        int maxY = Math.min(bounds.y + bounds.height, sizeY);
        
        for (int j = minY; j < maxY; j++)
        {
            int offset = j * sizeX + minX;
            for (int i = minX; i < maxX; i++, offset++)
                if (mask[offset] != 0)
                {
                    if (path.contains(i, j))
                    {
                        double value = Array1DUtil.getValue(data, offset, dataType);
                        inSum += (value - min) / max_min;
                        inCpt++;
                    }
                }
        }
        
        return inSum / inCpt;
    }
    
    @Override
    public double isColliding(OldControlPoint<?> v)
    {
        return isInside(v.position);
    }
    
    /**
     * Tests whether the given point is inside the contour, and if so returns the penetration depth
     * of this point. <br>
     * This methods computes the number of intersections between the contour and a semi-infinite
     * line starting from the contour center and passing through the given point. The point is thus
     * considered inside if the number of intersections is odd (Jordan curve theorem).<br>
     * Implementation note: the AWT Line2D class only provides a "segment to segment" intersection
     * test instead of a "semi-infinite line to segment" test, meaning that one must "fake" a
     * semi-infinite line using a big segment. This is done by building a segment originating from
     * the given point P and leaving in the opposite direction of the contour center C. The full
     * segment can be written in algebraic coordinates as
     * 
     * <pre>
     * [PQ] where Q = P + n * CP
     * </pre>
     * 
     * , where n is chosen arbitrarily large.
     * 
     * @param c
     *            a contour
     * @param p
     *            a point to test
     * @return true if the point is inside the contour
     */
    public double isInside(Vector2D p)
    {
        int n = 10000;
        
        Vector2D q = new Vector2D();
        // q <= cp * n + p
        q.set(p).subtract(getCenter()).scale(n).add(p);
        
        int nb = 0;
        int nbPtsM1 = getNbPoints() - 1;
        double dist = 0, minDist = Double.MAX_VALUE;
        
        // all points but the last
        for (int i = 0; i < nbPtsM1; i++)
        {
            Vector2D p1 = getControlPoint(i).position;
            Vector2D p2 = getControlPoint(i + 1).position;
            
            if (Vector2D.segmentsIntersect(p1, p2, p, q))
            {
                nb++;
                dist = Vector2D.getPointToLineDistance(p, p1, p2);
                if (dist < minDist) minDist = dist;
            }
        }
        
        // last point
        Vector2D p1 = getControlPoint(nbPtsM1).position;
        Vector2D p2 = getControlPoint(0).position;
        if (Vector2D.segmentsIntersect(p1, p2, p, q))
        {
            nb++;
            dist = Vector2D.getPointToLineDistance(p, p1, p2);
            if (dist < minDist) minDist = dist;
        }
        
        // return (nb % 2) == 0;
        return (nb % 2 == 1) ? minDist : 0.0;
        
    }
    
    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {
        if (getT() != canvas.getPositionT()) return;
        
        super.paint(g, sequence, canvas);
        
        synchronized (path)
        {
            g.draw(path);
        }
    }
    
    public void removeAllControlPoints()
    {
        controlPoints.clear();
    }
    
    public void updateNormals()
    {
        synchronized (controlPoints)
        {
            int n = controlPoints.size();
            
            // first point
            {
                Vector2D p1 = controlPoints.get(n - 1).position;
                Vector2D p2 = controlPoints.get(1).position;
                controlPoints.get(0).normal.set(p2.getY() - p1.getY(), p1.getX() - p2.getX()).normalize(2);
            }
            
            // middle points
            for (int i = 1; i < n - 1; i++)
            {
                Vector2D p1 = controlPoints.get(i - 1).position;
                Vector2D p2 = controlPoints.get(i + 1).position;
                controlPoints.get(i).normal.set(p2.getY() - p1.getY(), p1.getX() - p2.getX()).normalize(2);
            }
            
            // last point
            {
                Vector2D p1 = controlPoints.get(n - 2).position;
                Vector2D p2 = controlPoints.get(0).position;
                controlPoints.get(n - 1).normal.set(p2.getY() - p1.getY(), p1.getX() - p2.getX()).normalize(2);
            }
        }
    }
    
    public class PolygonTopologyOperator implements TopologyOperator
    {
        @Override
        public void reSample() throws TopologyException
        {
            if (getVolume(false) < getMinimumVolume()) throw new TopologyException(Polygon.this, new Contour[] {});
            
            double minLength = getResolution() * MIN_RESAMPLE_FACTOR;
            double maxLength = getResolution() * MAX_RESAMPLE_FACTOR;
            
            // optimization to avoid multiple points.size() calls (WARNING: n must
            // be updated manually whenever points is changed)
            int n = controlPoints.size();
            
            checkSelfIntersection();
            
            // update the number of total points
            n = controlPoints.size();
            
            boolean noChange = false;
            
            while (noChange == false)
            {
                noChange = true;
                
                // all points but the last
                for (int i = 0; i < n - 1; i++)
                {
                    if (n < 4) return;
                    
                    Vector2D pt1 = controlPoints.get(i).position;
                    Vector2D pt2 = controlPoints.get(i + 1).position;
                    
                    double distance = pt1.getL2Distance(pt2);
                    
                    if (distance < minLength)
                    {
                        noChange = false;
                        pt2.set((pt1.getX() + pt2.getX()) * 0.5, (pt1.getY() + pt2.getY()) * 0.5, 0);
                        controlPoints.remove(i);
                        i--; // comes down to i-1+1 when looping
                        n--;
                    }
                    else if (distance > maxLength)
                    {
                        noChange = false;
                        
                        controlPoints.add(i + 1, new OldControlPoint<Vector2D>(new Vector2D((pt1.getX() + pt2.getX()) * 0.5, (pt1.getY() + pt2.getY()) * 0.5)));
                        i++; // comes down to i+=2 when looping
                        n++;
                    }
                }
                
                // last point
                Vector2D pt1 = controlPoints.get(n - 1).position;
                Vector2D pt2 = controlPoints.get(0).position;
                
                if (pt1.getL2Distance(pt2) < minLength)
                {
                    noChange = false;
                    pt2.set((pt1.getX() + pt2.getX()) * 0.5, (pt1.getY() + pt2.getY()) * 0.5, 0);
                    controlPoints.remove(n - 1);
                    n--;
                }
                else if (pt1.getL2Distance(pt2) > maxLength)
                {
                    noChange = false;
                    controlPoints.add(new OldControlPoint<Vector2D>(new Vector2D((pt1.getX() + pt2.getX()) * 0.5, (pt1.getY() + pt2.getY()) * 0.5)));
                    n++;
                }
            }
            
            updateNormals();
        }
        
        /**
         * Checks whether the contour is self-intersecting. Depending on the given parameters, a
         * self-intersection can be considered as a loop or as a contour division.
         * 
         * @param minSpacing
         *            the distance threshold between non-neighboring points to detect
         *            self-intersection
         * @param minArea
         *            if a self-intersection is detected, this value specifies if the new contours
         *            are kept or eliminated
         * @return null if either no self-intersection is detected or if one of the new contours is
         *         too small, or an array of Contour2Ds with 0 elements if both contours are too
         *         small, and 2 elements if both contours are viable
         * @throws TopologyException
         */
        private void checkSelfIntersection() throws TopologyException
        {
            int i = 0, j = 0, n = controlPoints.size();
            
            Vector2D p_i = null, p_j = null;
            
            boolean intersection = false;
            
            for (i = 0; i < n; i++)
            {
                p_i = controlPoints.get(i).position;
                
                for (j = i + 2; j < n - 1; j++)
                {
                    p_j = controlPoints.get(j).position;
                    
                    intersection = (p_i.getL2Distance(p_j) < getResolution());
                    
                    if (intersection)
                    {
                        // deal with the special case that i and j are 2 points away
                        
                        if (i == 0 && j == n - 2)
                        {
                            n--;
                            controlPoints.remove(n);
                            intersection = false;
                        }
                        else if (i == 1 && j == n - 1)
                        {
                            controlPoints.remove(0);
                            n--;
                            intersection = false;
                        }
                        else if (j == i + 2)
                        {
                            controlPoints.remove(i + 1);
                            n--;
                            intersection = false;
                        }
                    }
                    
                    if (intersection) break;
                }
                if (intersection) break;
            }
            
            if (!intersection) return;
            
            Polygon c1 = new Polygon(resolution, timeStep);
            
            Polygon c2 = new Polygon(resolution, timeStep);
            
            int nPoints = j - i;
            for (int p = 0; p < nPoints; p++)
            {
                Vector2D pp = controlPoints.get(p + i).position;
                c1.controlPoints.add(new OldControlPoint<Vector2D>(pp));
            }
            
            c1.setZ(getZ());
            c1.setT(getT());
            
            nPoints = i + n - j;
            for (int p = 0, pj = p + j; p < nPoints; p++, pj++)
            {
                Vector2D pp = controlPoints.get(pj < n ? pj : pj - n).position;
                c2.controlPoints.add(new OldControlPoint<Vector2D>(pp));
            }
            
            c2.setZ(getZ());
            c2.setT(getT());
            
            // determine whether the intersection is a loop or a division
            // rationale: check the normal of the two colliding points (i & j)
            // if they point away from the junction => division
            // if they point towards the junction => loop
            
            Vector2D n_i = controlPoints.get(i).normal;
            Vector2D v_ij = new Vector2D(p_j.getX() - p_i.getX(), p_j.getY() - p_i.getY());
            
            if (n_i.dot(v_ij) < 0)
            {
                // division => keep c1 and c2 if their size is ok
                
                double c1area = c1.getVolume(false), c2area = c2.getVolume(false);
                
                // if only one of the two children has a size lower than minArea, then the division
                // should be considered as an artifact loop, the other child thus is the new contour
                
                if (c1area > getMinimumVolume())
                {
                    if (c2area > getMinimumVolume()) throw new TopologyException(Polygon.this, new Contour[] { c1, c2 });
                    
                    controlPoints.clear();
                    controlPoints.addAll(c1.controlPoints);
                }
                else
                {
                    if (c2area < getMinimumVolume()) throw new TopologyException(Polygon.this, new Contour[] {});
                    
                    controlPoints.clear();
                    controlPoints.addAll(c2.controlPoints);
                }
            }
            else
            {
                // loop => keep only the contour with correct orientation
                // => the contour with a positive algebraic area
                
                if (c1.getVolume(true) < 0)
                {
                    // c1 is the outer loop => keep it
                    controlPoints.clear();
                    controlPoints.addAll(c1.controlPoints);
                }
                else
                {
                    // c1 is the inner loop => keep c2
                    controlPoints.clear();
                    controlPoints.addAll(c2.controlPoints);
                }
            }
            
            // if code runs here, exit normally
        }
    }
}
