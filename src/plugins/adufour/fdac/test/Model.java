package plugins.adufour.fdac.test;

import javax.vecmath.Vector3d;

public abstract class Model
{
	protected double weight;
	
    public abstract Vector3d computeForces(ControlPoint cp);
    
    public void setWeight(double weight) {
		this.weight = weight;
	}
    
    public double getWeight() {
		return weight;
	}
}
