/**
 * 
 */
package plugins.adufour.fdac.test;

import javax.vecmath.Vector3d;

/**
 * @author morphospine
 *
 */
public class CurvatureRegularization2D extends Model {
	Vector3d forcesInternal;

	/** 
	 * @see
	 * plugins.adufour.fdac.test.Model#computeForces(plugins.adufour
	 * .fdac.test.ControlPoint)
	 */
	@Override
	public Vector3d computeForces(ControlPoint cp) {
		
		// Initialization
		forcesInternal = new Vector3d();

		// return a 3d vector
		// Get the first crown of neighbors

		for (ControlPoint neighbor : cp.edgeNeighbors.keySet()) {
			forcesInternal.add(neighbor.position);
		}

		double minSampling = cp.getMinimumSampling();

		forcesInternal.scale(weight / (cp.edgeNeighbors.size() * minSampling));

		return forcesInternal;
	}

}
