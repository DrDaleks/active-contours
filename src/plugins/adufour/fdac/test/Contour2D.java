package plugins.adufour.fdac.test;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Line2D;
import java.util.HashSet;

import icy.canvas.IcyCanvas;
import icy.painter.Overlay;
import icy.sequence.Sequence;
import icy.util.ColorUtil;

public class Contour2D extends Overlay
{
    private final HashSet<ControlPoint> points = new HashSet<ControlPoint>();
    
    private Color                          color  = ColorUtil.getRandomColor();
    
    public Contour2D(String name)
    {
        super(name);
    }
    
    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {
        g.setPaint(color);
        
        Line2D.Double line = new Line2D.Double();
        
        for (ControlPoint point : points)
        {
            line.x1 = point.position.x;
            line.y1 = point.position.y;
            
            for (ControlPoint neighbor : point.edgeNeighbors.keySet())
            {
                line.x2 = neighbor.position.x;
                line.y2 = neighbor.position.y;
                
                g.draw(line);
            }
        }
    }
}
