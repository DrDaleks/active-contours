package plugins.adufour.fdac.test;

import javax.vecmath.Point3d;

import icy.sequence.Sequence;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;

public abstract class DataModel extends Model
{
    protected final Sequence sequence;
    
    protected final int      frame;
    
    protected final int      channel;
    
    protected final int      width;
    
    protected final int      height;
    
    protected final int      depth;
    
    protected final DataType dataType;
    
    public DataModel(Sequence sequence, int t, int c)
    {
        this.sequence = sequence;
        this.frame = t;
        this.channel = c;
        this.width = sequence.getWidth();
        this.height = sequence.getHeight();
        this.depth = sequence.getSizeZ();
        this.dataType = sequence.getDataType_();
    }
    
    /**
     * Calculates the image value at the given real coordinates using 3D linear interpolation
     * 
     * @param point
     *            the coordinates of the point in double-precision
     * @param t
     *            the frame where the data must be sampled (<b>no interpolation</b>)
     * @param c
     *            the channel where the data must be sampled (<b>no interpolation</b>)
     * @return the interpolated image value at the given coordinates
     */
    protected double getPixelValue(Point3d point, int t, int c)
    {
        double x = point.x, y = point.y, z = point.z;
        
        final int i = (int) Math.round(x);
        final int j = (int) Math.round(y);
        final int k = (int) Math.floor(z);
        
        if (i < 0 || i >= width - 1) return 0;
        if (j < 0 || j >= height - 1) return 0;
        if (k < 0 || k >= depth - 1) return 0;
        
        Object lowSlice = sequence.getDataXY(t, k, c);
        Object highSlice = sequence.getDataXY(t, k + 1, c);
        
        final double min = sequence.getChannelMin(c);
        final double max = sequence.getChannelMax(c);
        
        final int pixel = i + j * width;
        final int east = pixel + 1; // saves 3 additions
        final int south = pixel + width; // saves 1 addition
        final int southeast = south + 1; // saves 1 addition
        
        double value = 0;
        
        x -= i;
        y -= j;
        z -= k;
        
        final double mx = 1 - x;
        final double my = 1 - y;
        final double mz = 1 - z;
        
        value += mx * my * mz * Array1DUtil.getValue(lowSlice, pixel, dataType);
        value += x * my * mz * Array1DUtil.getValue(lowSlice, east, dataType);
        value += mx * y * mz * Array1DUtil.getValue(lowSlice, south, dataType);
        value += x * y * mz * Array1DUtil.getValue(lowSlice, southeast, dataType);
        value += mx * my * z * Array1DUtil.getValue(highSlice, pixel, dataType);
        value += x * my * z * Array1DUtil.getValue(highSlice, east, dataType);
        value += mx * y * z * Array1DUtil.getValue(highSlice, south, dataType);
        value += x * y * z * Array1DUtil.getValue(highSlice, southeast, dataType);
        
        // rescale to [0-1] on the fly
        return (value - min) / (max - min);
    }
    
}
