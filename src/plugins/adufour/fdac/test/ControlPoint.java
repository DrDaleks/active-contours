package plugins.adufour.fdac.test;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.concurrent.ArrayBlockingQueue;

import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * Class defining a N-dimensional contour control point
 * 
 * @author Alexandre Dufour
 */
public class ControlPoint
{
    public enum ControlPointType
    {
        FRONT, EDGE, INTERNAL
    }
    
    private final ControlPointType               type;
    
    private final double                         minimalSampling;
    
    private transient boolean                    isResampling           = false;
    
    final Point3d                                position               = new Point3d();
    
    final LinkedHashMap<ControlPoint, Double> frontNeighbors         = new LinkedHashMap<ControlPoint, Double>();
    
    final LinkedHashMap<ControlPoint, Double> edgeNeighbors          = new LinkedHashMap<ControlPoint, Double>();
    
    final LinkedHashMap<ControlPoint, Double> internalNeighbors      = new LinkedHashMap<ControlPoint, Double>();
    
    private final ArrayBlockingQueue<Vector3d>   lastKnownDisplacements = new ArrayBlockingQueue<Vector3d>(10);
    
    private final HashSet<Model>     models                 = new HashSet<Model>();
    
    /**
     * Creates a new control point of default type (edge) with specified initial sampling
     * 
     * @param dimension
     */
    public ControlPoint(Point3d position, double minimalSampling)
    {
        this(ControlPointType.EDGE, position, minimalSampling);
    }
    
    /**
     * Creates a new control point at the specified position
     * 
     * @param position
     */
    public ControlPoint(ControlPointType type, Point3d position, double minimalSampling)
    {
        this.type = type;
        this.minimalSampling = minimalSampling;
        this.position.set(position);
    }
    
    /**
     * Adds the specified model to this control point
     * 
     * @param model
     */
    public void addModel(Model model)
    {
        models.add(model);
    }
    
    public String toString()
    {
        return "Control point: " + position.toString();
    }
    
    /**
     * Evolves this control point according to its various models until convergence
     * 
     * @throws InterruptedException
     */
    public void evolve(double speed) throws InterruptedException
    {
        lastKnownDisplacements.clear();
        
        double minSampling = getMinimumSampling();
        
        double maxDisplacement = minSampling * speed;
        
        while (!hasConverged())
        {
            // Retrieve the oldest displacement
            Vector3d displacement = lastKnownDisplacements.poll();
            if (displacement == null)
            {
                // Create a new one and push it to the FIFO list
                displacement = new Vector3d();
                lastKnownDisplacements.put(displacement);
            }
            else
            {
                // Reset it
                displacement.set(0, 0, 0);
            }
            
            // Calculate all displacements
            for (Model model : models)
            {
                displacement.add(model.computeForces(this));
            }
            
            // Cap the displacements if too large
            double displacementLength = displacement.length();
            if (displacementLength > maxDisplacement)
            {
                displacement.scale(maxDisplacement / displacementLength);
            }
            
            // Finally, move the control point
            position.add(displacement);
        }
        
        // Adjust the contour sampling locally
        if (adjustSampling())
        {
            // if sampling has been adjusted, restart evolution
            evolve(speed);
        }
    }
    
    /**
     * Get the maximum possible displacement from the smallest sampling of the neighborhood
     * 
     * @return
     */
    public double getMinimumSampling()
    {
        double minSampling = Double.MAX_VALUE;
        for (Double sampling : edgeNeighbors.values())
            if (sampling < minSampling) minSampling = sampling;
        return minSampling;
    }
    
    private boolean adjustSampling()
    {
        boolean samplingHasChanged = false;
        
        for (ControlPoint neighbor : new LinkedHashSet<ControlPoint>(edgeNeighbors.keySet()))
        {
            // Retrieve the sampling for the current neighbor
            double sampling = edgeNeighbors.get(neighbor);
            
            // Make sure the sampling needs adjustment
            if (sampling < minimalSampling) continue;
            
            // **Sampling must be adjusted**
            
            // 1. Make sure the neighbor isn't already doing the job
            if (neighbor.isResampling) continue;
            
            // 2. Notify the neighbor I am now doing the job
            isResampling = true;
            
            // 3. Remove links with my neighbor
            removeNeighbor(neighbor);
            neighbor.removeNeighbor(this);
            
            // Halve the sampling
            sampling /= 2;
            
            // Place a new point between me and my neighbor
            Point3d newPosition = new Point3d();
            newPosition.interpolate(this.position, neighbor.position, 0.5);
            ControlPoint newPoint = new ControlPoint(type, newPosition, minimalSampling);
            // FIXME what type of neighbor should we add??
            
            // Creates links between me and my new neighbor
            this.addNeighbor(newPoint, sampling);
            newPoint.addNeighbor(this, sampling);
            
            // Creates links between my old and new neighbors
            neighbor.addNeighbor(newPoint, sampling);
            newPoint.addNeighbor(neighbor, sampling);
            
            // FIXME Adding neighbors like this will not work (properly) in 3D
            
            isResampling = false;
            samplingHasChanged = true;
        }
        
        return samplingHasChanged;
    }
    
    public Vector3d computeNormal()
    {
        Vector3d normal = new Vector3d();
        
        for (ControlPoint neighbor : edgeNeighbors.keySet())
            normal.sub(neighbor.position);
        
        normal.scale(1.0 / edgeNeighbors.size());
        
        return normal;
    }
    
    private void addNeighbor(ControlPoint controlPoint, double sampling)
    {
        switch (controlPoint.type)
        {
        case EDGE:
            edgeNeighbors.put(controlPoint, sampling);
            break;
        case FRONT:
            frontNeighbors.put(controlPoint, sampling);
            break;
        case INTERNAL:
            internalNeighbors.put(controlPoint, sampling);
            break;
        }
    }
    
    private void removeNeighbor(ControlPoint controlPoint)
    {
        switch (controlPoint.type)
        {
        case EDGE:
            edgeNeighbors.remove(controlPoint);
            break;
        case FRONT:
            frontNeighbors.remove(controlPoint);
            break;
        case INTERNAL:
            internalNeighbors.remove(controlPoint);
            break;
        }
    }
    
    public boolean hasConverged()
    {
        if (lastKnownDisplacements.remainingCapacity() > 0) return false;
        
        Vector3d cumulativeDisplacement = new Vector3d();
        for (Vector3d disp : lastKnownDisplacements)
            cumulativeDisplacement.add(disp);
        
        double epsilon = getMinimumSampling() / 3;
        return (cumulativeDisplacement.lengthSquared() < epsilon);
    }
}
