package plugins.adufour.fdac.test;

import javax.vecmath.Vector3d;

import icy.sequence.Sequence;

public class ChanVeseMumfordShah extends DataModel
{
    public ChanVeseMumfordShah(Sequence sequence, int t, int c)
    {
        super(sequence, t, c);
    }
    
    @Override
    public Vector3d computeForces(ControlPoint cp)
    {
        Vector3d cvms = new Vector3d();
        double value, inDiff2, outDiff2;
        double mean_in = 1; // FIXME calculate interior mean!
        double mean_out = 0; // FIXME calculate exterior mean!
        
        double lambda = (1 / Math.max(mean_out * 2, mean_in));
        
        value = getPixelValue(cp.position, frame, channel);
        inDiff2 = (value - mean_in) * (value - mean_in);
        outDiff2 = (value - mean_out) * (value - mean_out);

        cvms.set(cp.computeNormal());
        cvms.scale(cp.getMinimumSampling() * weight * (lambda * outDiff2 - inDiff2 / lambda));
        
        return cvms;
    }
}
