package plugins.adufour.fdac.util;

import icy.math.ArrayMath;
import icy.math.Scaler;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.type.DataType;
import icy.type.collection.array.Array1DUtil;

import javax.vecmath.Point3d;
import javax.vecmath.Point3i;

/**
 * Utility class allowing to sample sequence information at non-integer locations using linear
 * interpolation of the neighborhood data
 * 
 * @author Alexandre Dufour
 * 
 */
public class SequenceSampler
{
    public final Point3i dimensions = new Point3i();
    
    public final Point3d minBounds  = new Point3d(), maxBounds = new Point3d();
    
    private double[][]   dataZ_XY;
    
    /**
     * Creates a new sampler for the given sequence. If necessary, the sequence will be converted to
     * type {@link DataType#DOUBLE}
     * 
     * @param sequence
     * @param rescale
     *            true if the data should be rescaled to the range [0, max value / max value of the
     *            input type] (warning: not necessarily [0,1])
     * @param smartRescale
     *            true if the data should be rescaled to [0,1] with 99.9999% min/max values (note:
     *            setting this parameter to true discards the <code>rescale</code> parameter
     */
    public SequenceSampler(Sequence sequence, int t, int c, boolean rescale, boolean smartRescale)
    {
        setSequence(sequence, t, c, rescale, smartRescale);
    }
    
    /**
     * Gets the sequence being sampled
     * 
     * @return
     */
    public double[][] getData()
    {
        return dataZ_XY;
    }
    
    /**
     * Sets the sequence to sample
     * 
     * @param sequence
     * @param rescale
     *            true if the data should be rescaled to the [0-1] range
     */
    public void setSequence(Sequence sequence, int t, int c, boolean rescale, boolean smartRescale)
    {
        Sequence converted;
        
        if (sequence.getDataType_() == DataType.DOUBLE && !rescale)
        {
            converted = sequence;
        }
        else
        {
            if (smartRescale)
            {
                // normalize the data "intelligently"
                // => take the 99.9999% min and max
                int slices = sequence.getSizeZ();
                int sliceSize = sequence.getSizeX() * sequence.getSizeY();
                boolean signed = sequence.isSignedDataType();
                
                double[] zxy = new double[sliceSize * slices];
                for (int z = 0; z < slices; z++)
                    Array1DUtil.arrayToDoubleArray(sequence.getDataXY(t, z, c), 0, zxy, z * sliceSize, sliceSize, signed);
                
                int pseudoMinIndex = (int) (zxy.length * 0.0001);
                int pseudoMaxIndex = (int) (zxy.length * 0.9999);
                double pseudoMin = ArrayMath.select(pseudoMinIndex, zxy);
                double pseudoMax = ArrayMath.select(pseudoMaxIndex, zxy);
                
                converted = SequenceUtil.convertToType(sequence, DataType.DOUBLE, new Scaler(pseudoMin, pseudoMax, 0.0, 1.0, false, false));
            }
            else
            {
                converted = SequenceUtil.convertToType(sequence, DataType.DOUBLE, rescale);
            }
        }
        
        // hold a direct pointer to the data to avoid sequence synchronization issues
        dataZ_XY = converted.getDataXYZAsDouble(t, c);
        dimensions.set(converted.getSizeX(), converted.getSizeY(), converted.getSizeZ());
        minBounds.set(0, 0, 0);
        maxBounds.set(sequence.getSizeX() - 1, sequence.getSizeY() - 1, sequence.getSizeZ() - 1);
    }
    
    /**
     * Computes the image value on current time and channel at the given real-valued position using
     * tri-linear interpolation of the voxel grid
     * 
     * @param x
     * @param y
     * @param z
     * @param t
     *            the time point on which the sample should be taken
     * @param c
     *            the channel on which the sample should be taken
     * @return
     * @throws ArrayIndexOutOfBoundsException
     *             if the given coordinates are out of the image bounds
     */
    public double getPixelValue(int i, int j, int k)
    {
        return dataZ_XY[k][i + j * dimensions.x];
    }
    
    public double getPixelValue(int slice, int offset)
    {
        return dataZ_XY[slice][offset];
    }
    
    /**
     * Computes the image value on current time and channel at the given real-valued position using
     * tri-linear interpolation of the voxel grid
     * 
     * @param x
     * @param y
     * @param z
     * @return the sampled pixel value
     * @throws ArrayIndexOutOfBoundsException
     *             if the given coordinates are out of the image bounds
     */
    public double getPixelValue(double x, double y, double z, boolean checkBounds)
    {
        if (checkBounds)
        {
            return getPixelValue(x, y, z, minBounds, maxBounds);
        }
        else
        {
            return getPixelValue(x, y, z);
        }
    }
    
    /**
     * Computes the image value on current time and channel at the given real-valued position using
     * tri-linear interpolation of the voxel grid
     * 
     * @param x
     * @param y
     * @param z
     * @return the sampled pixel value
     * @throws ArrayIndexOutOfBoundsException
     *             if the given coordinates are out of the image bounds
     */
    public double getPixelValue(double x, double y, double z)
    {
        final int i = (int) Math.floor(x);
        final int j = (int) Math.floor(y);
        final int k = (int) Math.floor(z);
        
        return getPixelValue(i, j, k, x, y, z);
    }
    
    /**
     * Computes the image value on current time and channel at the given real-valued position using
     * tri-linear interpolation of the voxel grid
     * 
     * @param x
     * @param y
     * @param z
     * @param minBounds
     *            the minimum bounds of the sampling area
     * @param maxBounds
     *            the maximum bounds of the sampling area
     * @return the sampled pixel value, and 0 if the pixel is not within the given bounds
     */
    public double getPixelValue(double x, double y, double z, Point3d minBounds, Point3d maxBounds)
    {
        final int i = (int) Math.floor(x);
        final int j = (int) Math.floor(y);
        final int k = (int) Math.floor(z);
        
        if (i < minBounds.x || i >= maxBounds.x || j < minBounds.y || j >= maxBounds.y || k < minBounds.z || k >= maxBounds.z) return 0;
        
        return getPixelValue(i, j, k, x, y, z);
    }
    
    /**
     * Computes the image value on current time and channel at the given real-valued position using
     * tri-linear interpolation of the voxel grid
     * 
     * @param x
     * @param y
     * @param z
     * @return the sampled pixel value
     */
    public double getPixelValue(int i, int j, int k, double x, double y, double z)
    {
        final int pixel = i + j * dimensions.x;
        final int east = pixel + 1; // saves 3 additions
        final int south = pixel + dimensions.x; // saves 1 addition
        final int southeast = south + 1; // saves 1 addition
        
        double[] currSlice = dataZ_XY[k];
        double[] nextSlice = dataZ_XY[k + 1];
        
        double value = 0;
        
        x -= i;
        y -= j;
        z -= k;
        
        final double mx = 1 - x;
        final double my = 1 - y;
        final double mz = 1 - z;
        
        value += mx * my * mz * currSlice[pixel];
        value += x * my * mz * currSlice[east];
        value += mx * y * mz * currSlice[south];
        value += x * y * mz * currSlice[southeast];
        value += mx * my * z * nextSlice[pixel];
        value += x * my * z * nextSlice[east];
        value += mx * y * z * nextSlice[south];
        value += x * y * z * nextSlice[southeast];
        
        return value;
    }
    
    @Override
    protected void finalize() throws Throwable
    {
        dataZ_XY = null;
        super.finalize();
    }
}
